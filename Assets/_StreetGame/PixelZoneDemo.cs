﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PixelZoneDemo : MonoBehaviour {

    public MeshRenderer renderer;
    public List<PixelZone> zones;

    void Start()
    {

        //UshortExemple();
        TextureExemple();
        RealTextureExemple((Texture2D)this.renderer.material.mainTexture, ref this.zones);
    }
    private static void RealTextureExemple(Texture2D texture, ref List<PixelZone> zones)
    {
        if (texture == null) return;
        zones = PixelZone.GetZone<Color>(Color.black, (x, y) => { return texture.GetPixel(x, y); }, texture.width, texture.height);
    }

    private static void TextureExemple()
    {
        Texture2D texture = new Texture2D(20, 10, TextureFormat.ARGB32, false);
        for (int y = 0; y < 10; y++)
        {
            for (int x = 0; x < 20; x++)
            {
                texture.SetPixel(x, y, Color.black);
            }
        }
        texture.SetPixel(3, 15, Color.green);
        texture.SetPixel(6, 2, Color.yellow);
        PixelZone.GetZone<Color>(Color.black, (x, y) => { return texture.GetPixel(x, y); }, 20, 10);
    }

    private static void UshortExemple()
    {
        ushort[,] elements = new ushort[10, 20];
        elements[3, 15] = 315;
        elements[6, 2] = 62;
        PixelZone.GetZone<ushort>((ushort)0, (x, y) => { return elements[y, x]; }, 20, 10);
    } 
}
