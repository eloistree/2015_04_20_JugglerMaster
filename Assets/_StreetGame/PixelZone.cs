﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


/// <summary>
/// Determine the zone in the table given(Texture, Depth ..)
/// And store it as PixelZone
/// </summary>
 [System.Serializable]
public class PixelZone  {


    public Vector2 CenterCoord;
    public Vector2 CenterNormalized;
    public Vector2 FarestCoord;
    public Vector2 NearestCoord;
    public float CenterToNearestBorder;
    
    public float CenterToFarestBorder;
    
    public int BorderPixelCount;
    public float ContainerWidth;
    public float ContainerHeight;

    public Vector2 FarestCoordNormalized
    {
        get
        {
            Vector2 val = FarestCoord;
            val.x /= ContainerWidth;
            val.y /= ContainerHeight;
            val.y = 1f - val.y;
            return val;
        }
    }
    public Vector2 NearestCoordNormalized
    {
        get
        {
            Vector2 val = NearestCoord;
            val.x /= ContainerWidth;
            val.y /= ContainerHeight;
            val.y = 1f - val.y;
            return val;
        }
    }
    public float CenterToNearestBorderNormalized
    {
        get
        {

            return Vector2.Distance(CenterNormalized, NearestCoordNormalized);
        }
    }
    public float CenterToFarestBorderNormalized
    {
        get
        {

            return Vector2.Distance(CenterNormalized, FarestCoordNormalized);
        }
    }
     
     //public int PixelBorderCount { get { return borders.Count; } }
    //public List<Vector2> borders = new List<Vector2>(); 

    public  delegate Elem GetValue<Elem>(int x, int y);

    public static Pool<Vector2> poolExploration = new Pool<Vector2>(5000);
    public static Pool<Vector2> poolBorder = new Pool<Vector2>(10000);





    //Idea to add : do the same of GetZone but with Range of elements to deny
    public static List<PixelZone> GetZone<Elem>( Elem emptyValue, GetValue<Elem> accessor, int width, int height)
    {
        List<PixelZone> result=  new List<PixelZone>();
        bool[,] exploredPixel = new bool[height, width];
        List<Vector2> tmpZoneBorder = new List<Vector2>();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++) 
            {

                if ( ! exploredPixel[y,x] && !accessor(x, y).Equals(emptyValue)) {
                    //Zone found 
                    tmpZoneBorder.Clear();
                    PixelZone zone = new PixelZone();
                    //Explore the zone size
                    ExploreZone<Elem>(ref accessor, ref exploredPixel, ref x, ref y, ref width, ref height, ref emptyValue, ref tmpZoneBorder);
                    
                    //Get info from list of vector
                    GetInfoFromVectors( ref tmpZoneBorder, ref zone, ref width, ref height);
 
                    //Add of the zone
                    result.Add(zone);
                }

                exploredPixel[y, x] = true;
            }
        }

        return result;
    }

    private static void GetInfoFromVectors( ref List<Vector2> tmpZoneBorder, ref PixelZone zone, ref int width, ref int height)
    {
        //found the center
        Vector2 center = Vector2.zero;
        for (int i = 0; i < tmpZoneBorder.Count; i++)
			{
                center += tmpZoneBorder[i];
			} 
        center /= tmpZoneBorder.Count;

        //found the range
        float farestVal = 0f, nearestVal = float.MaxValue;
        Vector2 farestCoord =center, nearestCoord = center;
        for (int i = 0; i < tmpZoneBorder.Count; i++)
        {
            float length = Vector2.Distance(tmpZoneBorder[i], center);
            if (length > farestVal)
            {
                farestVal = length;
                farestCoord = tmpZoneBorder[i];
            }
            if (length < nearestVal)
            {
                nearestVal = length;
                nearestCoord = tmpZoneBorder[i];
            }
        }

        zone.CenterCoord = center;
        zone.CenterNormalized.x = center.x / width;
        zone.CenterNormalized.y = 1f - center.y / height;

        zone.CenterToNearestBorder = nearestVal;
        zone.CenterToFarestBorder = farestVal;
        zone.FarestCoord = farestCoord;
        zone.NearestCoord = nearestCoord;
        zone.ContainerHeight = height;
        zone.ContainerWidth = width;
        zone.BorderPixelCount = tmpZoneBorder.Count;
    }
    private static Queue<Vector2> nextToCome = new Queue<Vector2>();
    private static void  ExploreZone<Elem>(ref GetValue<Elem> accessor, ref bool[,] visitedCoord, ref int x, ref int y, ref int width, ref int height, ref Elem emptyValue, ref List<Vector2> tmpZoneBorder)
    {
        
        tmpZoneBorder.Clear();

        nextToCome.Clear();
        nextToCome.Enqueue(new Vector2(x,y));
        while(nextToCome.Count>0)
        {
            Vector2 next = nextToCome.Dequeue();
            bool isOut = (x < 0 || x >= width) || (y < 0 || y >= height);
            if (isOut) continue;

            int nx = (int)next.x, ny = (int)next.y;
            bool alreadyVisited = visitedCoord[ny,nx];
            visitedCoord[ny, nx] = true;
            bool isEmpty = accessor(nx,ny).Equals(emptyValue);

            if (!isOut && !alreadyVisited && !isEmpty) {
                //tmpZone.Add(next);


                Vector2 neighbor = Vector2.zero;
                bool isBorder = false;

                IfNotEmptyAddToQueue<Elem>(ref accessor, ref width, ref height, ref emptyValue, ref nextToCome, ref isBorder, nx, ny + 1);
                IfNotEmptyAddToQueue<Elem>(ref accessor, ref width, ref height, ref emptyValue, ref nextToCome, ref isBorder, nx-1, ny);
                IfNotEmptyAddToQueue<Elem>(ref accessor, ref width, ref height, ref emptyValue, ref nextToCome, ref isBorder, nx, ny - 1);
                IfNotEmptyAddToQueue<Elem>(ref accessor, ref width, ref height, ref emptyValue, ref nextToCome, ref isBorder, nx+1, ny );

                if (isBorder) {
                    Vector2 pos = poolBorder.GetNext();
                    pos.x = nx;
                    pos.y = ny;
                    tmpZoneBorder.Add(pos);

                }
            }
        }
    }

    private static void IfNotEmptyAddToQueue<Elem>(ref GetValue<Elem> accessor, ref int width, ref int height, ref Elem emptyValue, ref Queue<Vector2> nextToCome, ref bool isBorder, int nx, int ny)
    {
        if (IsNeighborEmpty(ref accessor, nx, ny, ref width, ref height, ref emptyValue))
            isBorder |= true;
        else
        {
            
            Vector2 pos = poolExploration.GetNext();
            pos.x = nx;
            pos.y = ny; 
            nextToCome.Enqueue(pos);
        } 
    }

    private static bool IsNeighborEmpty<Elem>( ref GetValue<Elem> accessor, int x, int y, ref int width, ref int height, ref Elem emptyValue)
    {
        bool isOut = (x < 0 || x >= width) || (y < 0 || y >= height);
        if (isOut) return true;
        bool isEmpty = accessor(x, y).Equals(emptyValue);
        return isEmpty;
    }






  

}

public class Pool<T> where T : new ()
{
    private T[] allocatedValue;
    private int cursor;
    public Pool(int allocated ) 
    {
        allocatedValue = new T[allocated];
        for (int i = 0; i < allocated; i++)
        {
            allocatedValue[i] = new T();
            
        }
        
    }

    public T GetNext() {
        int c = (++cursor) % allocatedValue.Length;
        return allocatedValue[c];

    }
}
