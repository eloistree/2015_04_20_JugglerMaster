﻿using UnityEngine;
using System.Collections;





public class BossLife : ColoredLife {




    public SpriteRenderer centerSprite;
    public SpriteRenderer upSprite;
    public SpriteRenderer leftSprite;
    public SpriteRenderer rightSprite;

    protected override void RefreshLifeColor(Color life)
    {
        base.RefreshLifeColor(life);
        if (centerSprite != null)
            centerSprite.color = life;
        if (upSprite != null)
            upSprite.color = greenBlueUpLife;
        if (leftSprite != null)
            leftSprite.color = greenRedLeftLife;
        if (rightSprite != null)
            rightSprite.color = redBlueRightLife;
    }
}
