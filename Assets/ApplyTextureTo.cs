﻿using UnityEngine;
using System.Collections;

public class ApplyTextureTo : MonoBehaviour {

    public MeshRenderer meshRenderer;
    public CreateTextureFromKinectDepth textureCreated;


    void Update() 
    {
        if (textureCreated.texture != null) {
            meshRenderer.material.SetTexture(0, textureCreated.texture);     
        }
   }

}
