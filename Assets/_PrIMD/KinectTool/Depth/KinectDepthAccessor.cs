﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
using System;

public class KinectDepthAccessor : MonoBehaviour
{

    //    public DepthSourceManager depthSource;
    private KinectSensor sensor;
    private bool inverseHorizontal;
    public int divideBy=2;
    //  public ushort[] origineDepthValue;


    //private ushort[,] _depth;

    //public ushort[,] Depth
    //{
    //    get { return _depth; }
    //    private set { _depth = value; }
    //}

    private int _depthWidth;
    public int DepthWidth
    {
        get { return _depthWidth; }
        private set { _depthWidth = value; }
    }
    private int _depthHeight;
    public int DepthHeight
    {
        get { return _depthHeight; }
        private set { _depthHeight = value; }
    }
    private int _kinectDepthHeight;
    private int _kinectDepthWidth;
    public int KinectDepthWidth
    {
        get { return _kinectDepthWidth; }
        private set { _kinectDepthWidth = value; }
    }
    public int KinectDepthHeight
    {
        get { return _kinectDepthHeight; }
        private set { _kinectDepthHeight = value; }
    }

    public bool withFilter = true;
    [Range(0,ushort.MaxValue)]
    public int filterMin, filterMax = 4000;

    void Start()
    {

        sensor = KinectSensor.GetDefault();

        FrameDescription descri = sensor.DepthFrameSource.FrameDescription;
        KinectDepthHeight = descri.Height;
        KinectDepthWidth = descri.Width;
        DepthWidth = descri.Width / divideBy;
        DepthHeight = descri.Height / divideBy;
        DepthRaw = new ushort[DepthWidth * DepthHeight];
       
    }
    public ushort[] DepthRaw;
    //public int averageNs;
    //public int sumNs;
    //public int count;
    void Update()
    {
        KinectManager kinect = KinectManager.Instance;
        //long start = DateTime.Now.Ticks;
        ushort[] kinDepth = kinect.GetRawDepthMap();

        
           // DepthRaw = new ushort[DepthWidth * DepthHeight];
            for (int y = 0,index = 0,  kinY=0,  indY=0; y < DepthHeight; y++)
            {
                indY = y * DepthWidth;
                kinY = indY * divideBy*2 ;
           
                for (int x = 0; x < DepthWidth; x++)
                {
                    index = x + indY;
                    ushort depthValue = kinDepth[x * divideBy +kinY];
                    if ( withFilter && ( depthValue < filterMin || depthValue > filterMax)) { 
                            DepthRaw[index] = 0;
                    }
                    else DepthRaw[index] = depthValue;
                    //if (depthValue > filterMin && depthValue < filterMax)
                    //    DepthRaw[index] = depthValue;
                }
            }
        //long stop = DateTime.Now.Ticks;
        //count++;
        //sumNs += (int)(stop - start);
        //averageNs = sumNs / count;
    }

    public ushort GetDepthValueAt(int x, int y)
    {
        return DepthRaw[x + y * DepthWidth];
    }

    public ushort GetDepthValueAt(Vector2 coord)
    {
        return DepthRaw[(int)coord.y + (int)coord.x * DepthWidth];
    }

    //public ushort[,] GetDepthCopy()
    //{
    //    return (ushort[,]) _depth.Clone();
    //}
}

///OBSELETE VERSION
//public class KinectDepthAccessor : MonoBehaviour {

//    public DepthSourceManager depthSource;
//    private KinectSensor sensor;
//    public ushort[] origineDepthValue;

//    private int _avrWidth;
//    public int AvrWidth
//    {
//        get { return _avrWidth; }
//        private set { _avrWidth = value; }
//    }
//    private int _avrHeight;
//    public int AvrHeight
//    {
//        get { return _avrHeight; }
//        private set { _avrHeight = value; }
//    }
//    private int _depthWidth;
//    public int DepthWidth
//    {
//        get { return _depthWidth; }
//        private set { _depthWidth = value; }
//    }
//    private int _depthHeight;
//    public int DepthHeight
//    {
//        get { return _depthHeight; }
//        private set { _depthHeight = value; }
//    }
    

//    [Range(0,ushort.MaxValue)]
//    public int from, to;

//    void Start() {

//        sensor = KinectSensor.GetDefault();

//        FrameDescription descri = sensor.DepthFrameSource.FrameDescription;
//        DepthWidth = descri.Width;
//        DepthHeight = descri.Height;

//    }
//    void Update () {

//        if (depthSource == null || sensor==null) return;
//        ushort from = (ushort) this.from;
//        ushort to = (ushort) this.to;

//        FrameDescription descri = sensor.DepthFrameSource.FrameDescription;
//        origineDepthValue = depthSource.GetData();
//        DepthWidth = descri.Width;
//        DepthHeight = descri.Height;
//    }


//    public ushort[] GetDepthvalue() {
//        return origineDepthValue;
//    }

//    public ushort GetDepthValueAt(int x, int y) 
//    {
//        int index = DoubleCoord(ref x, ref y, ref _depthWidth);
//        if(index<0 || index>= origineDepthValue.Length) return 0;
//        return origineDepthValue[index];
    
//    }

//    public int DoubleCoord(ref int x, ref int y ,ref int width) { return y * width + x; }
//}
