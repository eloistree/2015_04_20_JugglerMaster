﻿using UnityEngine;
using System.Collections;

public class HandsDisplayer : MonoBehaviour {

    public KinectGrabbingHand grapper;
    public Texture defaultTextureNormal;
    public Texture defaultTextureActive;

    public Texture handLeftTextureNormal;
    public Texture handLeftTextureActive;

    public Texture handRightTextureNormal;
    public Texture handRightTextureActive;


    public float widthHands =50f;
    public float heightHands=50f;


    public KinectGrabbingHand.ScreenPositionCustom [] handLeftCustom;
    public KinectGrabbingHand.ScreenPositionCustom [] handRightCustom;
    public float zDistance;
    public bool zFromShoulder=true;

    //public Texture leftHandNormal;
    //public Texture leftHandStay;
    //public Texture leftHandLoading;

    //public Texture rightHandStay;
    //public Texture rightHandLoading;
	// Use this for initialization
    void OnGUI() {
        if (grapper == null) return;
            DrawHand(grapper.handLeft, grapper.GetCustomScreenPosition(ref grapper.handLeft, handLeftCustom, zFromShoulder, 0));
            DrawHand(grapper.handRight, grapper.GetCustomScreenPosition(ref grapper.handRight, handRightCustom, zFromShoulder, 0));
     
    }

    private void DrawHand(KinectGrabbingHand.HandInfo hand, Vector3 screenPosition)
    {
        bool isLeft = hand.side== KinectGrabbingHand.HandInfo.Side.Left;
       // bool defautlInit = defaultTextureNormal != null && defaultTextureActive;
        bool leftInit = handLeftTextureNormal != null && handLeftTextureActive;
        bool rightInit = handRightTextureNormal != null && handRightTextureActive;

        Texture tNormal = !leftInit ? defaultTextureNormal : (isLeft ? handLeftTextureNormal : handRightTextureNormal);
        Texture tActive = !rightInit ? defaultTextureActive : (isLeft ? handLeftTextureActive : handRightTextureActive);
        Texture t = hand.isUsed ? tActive : tNormal; 
        if ( tNormal!=null && tActive!=null  && hand != null && hand.hasHandUp)
        {
            GUI.DrawTexture(new Rect(screenPosition.x - widthHands / 2f, Screen.height - screenPosition.y - heightHands / 2f, widthHands, heightHands), t);
        }
    }
        
}

