﻿/*Coded by PrIMD (primd42@gmail.com) for Kraken RealTime (http://krakenrealtime.com/)*/
using UnityEngine;
using System.Collections;

public abstract class KinectGrabbingHand : MonoBehaviour
{

    [System.Serializable]
    public class HandInfo
    {

        public enum Side { Left, Right }
        public Side side;
        public bool isUsed;
        public bool hasHandUp;
        public bool isHandNotMoving;
        public bool isClosed;

 
        public Vector3 startOffSet;
        public Vector3 position;

        public Vector3 screenPositionNormalized;
        public Vector3 screenPosition;

        public Vector3 lastMovingPosition;
        public Vector3 lastFramePosition;
        public Vector3 direction;
        public Vector3 delta;

        public float handNotMovingSince;

        //Since how many time the hand is open
        public float couldBeReleaseSince;
        //Since how many time the hand is declared open
        public float isClosedSince;

        public float shoulderhandHeight;

        public float switchCoolDown;
        //NOT TO USE
        public float switchCoolDownCount;

    }



    public HandInfo handLeft;
    public HandInfo handRight;

    public float widthMeter = 1.5f;
    public float heightMeter = 0.8f;
    public float forwardMeter = 1f;
    public float shoulderMeter = 0.2f;

    public float handUpAtHeightFromShoulder = -0.2f;
    public float handDownAtHeightFromShoulder = -0.4f;

    public float distanceToBeMoving = 0.05f;
    public float timeToBeFixed = 0.7f;


    // The hand have to be release for at least x second in aim to be considered as open
    // It is use to compensate the tweaking of the hand state.
    // Set to 0 if useing a close state that do not tweak
    public bool antiHandCloseVariation = true;
    public float handReleaseMinTimeToBeOpen = 0.4f;

    public delegate void OnHandClosed(HandInfo handInfo);
    public OnHandClosed onHandClosed;
    public delegate void OnHandReleased(HandInfo handInfo);
    public OnHandReleased onHandReleased;
    public delegate void OnHandIsUsed(HandInfo handInfo);
    public OnHandIsUsed onHandIsUsed;


    public delegate bool StateChangeCondition(HandInfo hand);

    private StateChangeCondition inCondition;
    public void SetStartUsingIf_Condition(StateChangeCondition condition)
    {
        if (condition != null)
            this.inCondition = condition;

    }
    private StateChangeCondition outCondition;
    public void SetStopUsingIf_Condition(StateChangeCondition condition)
    {
        if (condition != null)
            this.outCondition = condition;

    }




    protected virtual void Start()
    {
        SetStopUsingIf_Condition(DefaultConditionToStopUsedHand);
        SetStartUsingIf_Condition(DefaultConditionToStartUsedHand);
    }

    private bool DefaultConditionToStartUsedHand(HandInfo hand)
    {
        return hand.hasHandUp && hand.handNotMovingSince>1.5f && hand.switchCoolDownCount<=0;
    }

    private bool DefaultConditionToStopUsedHand(HandInfo hand)
    {
        return !hand.hasHandUp || hand.isClosed || (hand.handNotMovingSince > 2.5f && hand.switchCoolDownCount <= 0);
    }



    private void Update()
    {
   
        UpdateHandsInfo();
        IsActiveAndSwitch(ref handLeft);
        IsActiveAndSwitch(ref handRight);


    }

    public bool IsActiveAndSwitch(ref HandInfo hand)
    {
        if (!hand.isUsed)
        {
            bool isReadyToBeUsed = inCondition(hand);
            if (isReadyToBeUsed)
            {
                hand.startOffSet = hand.position;
                hand.handNotMovingSince = 0f;
                hand.isUsed = true;
                hand.switchCoolDownCount = hand.switchCoolDown;
                //Debug.Log("On.. "+hand.side);
                if (onHandIsUsed != null)
                    onHandIsUsed(hand);
            }
        }
        else if (hand.isUsed && hand.switchCoolDownCount <= 0f)
        {
            bool isReadyToBeClosed = outCondition(hand);
            if (isReadyToBeClosed)
            {
                ResetHand(hand);
                //Debug.Log("Off.. " + hand.side);
                if (onHandIsUsed != null)
                    onHandIsUsed(hand);
            }
        }
        return hand.isUsed;
    }



    private void ResetHand(HandInfo hand)
    {
        //DEBUGINDICE//
        //hand.position = Vector3.zero;
        hand.startOffSet = Vector3.zero;
        //hand.lastMovingPosition = Vector3.zero;
        //hand.lastFramePosition = Vector3.zero;
        //hand.direction = Vector3.zero;
        //hand.delta = Vector3.zero;

        hand.handNotMovingSince = 0f;
        hand.isClosedSince = 0f;
        hand.isUsed = false;
        hand.switchCoolDownCount = 0.15f;
    }




    public abstract void GetHandsFromShouldersPosition(out Vector3 handLeftPos, out Vector3 handRightPos);
    public abstract void GetHandsClosedState(out bool handLeftClosed, out bool handRightClosed);


    protected void UpdateHandsInfo()
    {


        Vector3 handLeftPos;
        Vector3 handRightPos;
        bool handLeftClosed;
        bool handRightClosed;

        GetHandsFromShouldersPosition(out handLeftPos, out handRightPos);
        GetHandsClosedState(out handLeftClosed, out handRightClosed);

        handLeft.side = HandInfo.Side.Left;
        handRight.side = HandInfo.Side.Right;

        UpdateHand_PositionDirDeltaHeight(ref handLeft, ref handLeftPos);
        UpdateHand_PositionDirDeltaHeight(ref handRight, ref handRightPos);

        // Set the closed state of the hand (with anti varation if asked)
        UpdateHandsClosedState(ref handRight, handRightClosed);
        UpdateHandsClosedState(ref handLeft, handLeftClosed);
        // set and detecte if hand is moving and since how many time
        SetMovingState(ref handLeft);
        SetMovingState(ref handRight);

        //Is user hand is up

        handLeft.hasHandUp = IsHandUp(handLeft);
        handRight.hasHandUp = IsHandUp(handRight);

        //Prepare for the next update the last position
        handLeft.lastFramePosition = handLeft.position;
        handRight.lastFramePosition = handRight.position;

        SetHandScreenPosition(ref handLeft);
        SetHandScreenPosition(ref handRight);

        if (handLeft.switchCoolDownCount > 0f)
            handLeft.switchCoolDownCount -= Time.deltaTime;
        if (handRight.switchCoolDownCount > 0f)
            handRight.switchCoolDownCount -= Time.deltaTime;

    }

    private void UpdateHand_PositionDirDeltaHeight(ref HandInfo hand, ref Vector3 handNewPos)
    {
        //Set position
        hand.position = handNewPos;
        //Set hand height compare to shoulder
        hand.shoulderhandHeight = handNewPos.y;
        //set the delta movement of hand
        hand.delta = hand.position - hand.lastFramePosition;
        //Set the direction of the movement compare to the start offset
        hand.direction = hand.position - hand.startOffSet;
    }

    private void SetMovingState(ref HandInfo hand)
    {
        if (IsMoving(ref hand))
        {
            hand.lastMovingPosition = hand.position;
            hand.handNotMovingSince = 0f;
        }
        hand.handNotMovingSince += Time.deltaTime;
        hand.isHandNotMoving = hand.handNotMovingSince > timeToBeFixed;
    }

    private void UpdateHandsClosedState(ref HandInfo hand, bool isClosed)
    {
        if (antiHandCloseVariation)
        {
            if (!hand.isClosed && isClosed)
            {
                hand.isClosed = true;
                hand.couldBeReleaseSince = 0f;
                if (onHandClosed != null)
                    onHandClosed(hand);
            }
            else if (hand.isClosed && !isClosed)
            {
                hand.couldBeReleaseSince += Time.deltaTime;
                if (hand.couldBeReleaseSince > this.handReleaseMinTimeToBeOpen)
                {
                    hand.isClosed = false;
                    if (onHandReleased != null)
                        onHandReleased(hand);
                }
            }
            else if (hand.isClosed && isClosed)
            {
                hand.couldBeReleaseSince = 0f;
            }
        }
        else hand.isClosed = isClosed;

        if (hand.isClosed)
            hand.isClosedSince += Time.deltaTime;
        else hand.isClosedSince = 0f;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="hand"></param>
    private void SetHandScreenPosition(ref HandInfo hand)
    {
        float x, y, z;
        float offset = (hand.side == HandInfo.Side.Left) ? -shoulderMeter : shoulderMeter;

        x = Mathf.Clamp((hand.position.x + offset + widthMeter / 2f) / (widthMeter), 0f, 1f);
        y = Mathf.Clamp((hand.position.y + heightMeter / 2f) / heightMeter, 0f, 1f);
        z = Mathf.Clamp((hand.position.z / 2f) / forwardMeter, 0f, 1f);
        hand.screenPositionNormalized.x = x;
        hand.screenPositionNormalized.y = y;
        hand.screenPositionNormalized.z = z;
        hand.screenPosition.x = x * Screen.width;
        hand.screenPosition.y = y * Screen.height;

    }




    private bool IsMoving(ref HandInfo hand)
    {
        return Vector3.Distance(hand.position, hand.lastMovingPosition) > distanceToBeMoving;
    }

    private bool IsHandUp(HandInfo hand)
    {
        float height = hand.shoulderhandHeight;

        if (hand.hasHandUp == false && height > handUpAtHeightFromShoulder) { return true; }
        else if (hand.hasHandUp == true && height < handDownAtHeightFromShoulder) { return false; }
        else return hand.hasHandUp;

    }


    /// <summary>
    /// Should work, not tested yet (15/01/2015)
    /// </summary>
    /// <param name="hand"> The reference to the hand data to be treated</param>
    /// <param name="custom"> What kind of Screen position you want</param>
    /// <param name="zFromShouldToHand"> If true: the value = 0 when hand is next to shoulder, value = 1 when at forwardMeter Distance.</param>
    /// <returns></returns>
    public Vector3 GetCustomScreenPositionNormalized(ref HandInfo hand, ScreenPositionCustom [] customs, bool zFromShouldToHand = true)
    {
        float x, y, z;
        float offset = (hand.side == HandInfo.Side.Left) ? -shoulderMeter : shoulderMeter;
        float offsetNorm = offset/widthMeter;
        //float offsetCorrection = 1f;
        //if (hand.side == HandInfo.Side.Left && hand.position.x  > 0f) offsetCorrection = 1.5f;
        //if (hand.side == HandInfo.Side.Right && hand.position.x  < 0f) offsetCorrection = 1.5f;

        //         x = Mathf.Clamp((hand.position.x*offsetCorrection + offset + widthMeter / 2f) / (widthMeter), 0f, 1f);
            
        //        x = Mathf.Clamp((hand.position.x + widthMeter / 2f) / (widthMeter), 0f, 1f);
        x = Mathf.Clamp((hand.position.x + widthMeter / 2f) / (widthMeter), 0f, 1f);
        x = Mathf.Clamp(x+( (hand.side == HandInfo.Side.Right) ? offsetNorm*x: offsetNorm*(1f-x)), 0f, 1f);
        y = Mathf.Clamp((hand.position.y + heightMeter / 2f) / heightMeter, 0f, 1f);
        z = Mathf.Clamp((hand.position.z + (zFromShouldToHand ? 0f : forwardMeter) / 2f) / forwardMeter, 0f, 1f);



        Vector3 screenPosNorm =  new Vector3 (x,y,z);
        ApplyCustomEffect(ref screenPosNorm, customs);
        
        return screenPosNorm;
    }

  

    public Vector3 GetCustomScreenPosition(ref HandInfo hand, ScreenPositionCustom [] customs, bool zFromShouldToHand = true , float zUnityDistanceWanted = 0f )
    {
        Vector3 pos = GetCustomScreenPositionNormalized(ref hand, customs, zFromShouldToHand);
        pos.x *= Screen.width;
        pos.y *= Screen.height;
        pos.z *= zUnityDistanceWanted;
        return pos;
    }
    public enum ScreenPositionCustom { None, InverseX, InverseY, InverseZ, IgnoreX, IgnoreY, IgnoreZ, Inverse_Z_To_Y, Inverse_Z_To_X, Inverse_X_To_Y }
    
    private void ApplyCustomEffect(ref Vector3 val, ScreenPositionCustom[] customs)
    {
        if (customs == null) return;
        foreach (ScreenPositionCustom cust in customs)
        {
            switch (cust)
            {
                case ScreenPositionCustom.Inverse_Z_To_Y:
                    Inverse(ref val.z, ref val.y);
                    break;
                case ScreenPositionCustom.Inverse_Z_To_X:
                    Inverse(ref val.z, ref val.x);
                    break;
                case ScreenPositionCustom.Inverse_X_To_Y:
                    Inverse(ref val.x, ref val.y);
                    break;
                case ScreenPositionCustom.InverseX: val.x = 1f -val.x; break;
                case ScreenPositionCustom.InverseY: val.y = 1f - val.y; break;
                case ScreenPositionCustom.InverseZ: val.z = 1f - val.z; break;
                case ScreenPositionCustom.IgnoreX: val.x = 0; break;
                case ScreenPositionCustom.IgnoreY: val.y = 0; break;
                case ScreenPositionCustom.IgnoreZ: val.z = 0; break;
            }
        }
    }
    private void Inverse(ref float a, ref float b) { a = a + b; b = a - b; a = a - b; }

}




