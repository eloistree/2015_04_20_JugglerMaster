﻿using UnityEngine;
using System.Collections;

public class KinectIntraction_UIButtonSingleHand : MonoBehaviour {

    public KinectGrabbingHand hands;
    public enum SideType { Left, Right }
    public SideType whichHand = SideType.Right;
    public ScreenToIngameInteraction screenInteractor ;

    void Start()
    {
        if (screenInteractor == null)
        { 
            Debug.Log("The scrren interactior has to be define");
            Destroy(this); return; 
        }
        if (hands != null)
        {
            hands.onHandIsUsed += Press;
            hands.onHandIsUsed += Release;
        }
        
    }
    void OnDestroy()
    {
        if (hands != null)
        {
            hands.onHandIsUsed -= Press;
            hands.onHandIsUsed -= Release;
        }
    }

    void Update() 
    {
        
        screenInteractor.ScreenPosition = ScreenPosition;
    }

    private void Press(KinectGrabbingHand.HandInfo handInfo)
    {
        if (!IsTheGoodHand(ref handInfo)) return;
        if (handInfo.isUsed)
        {
           // screenInteractor.Press();
        }

    }
    private void Release(KinectGrabbingHand.HandInfo handInfo)
    {
        if (!IsTheGoodHand(ref handInfo)) return;
        if (handInfo.isUsed)
        {
         //   screenInteractor.Release();
        }

    }

    public  Vector3 ScreenPosition
    {
        get
        {
            if (hands == null) return Vector3.zero;
            KinectGrabbingHand.HandInfo hand = GetGoodHand();
            if (!hand.hasHandUp) return Vector3.zero;
            return hands.GetCustomScreenPosition(ref hand, null, true, 0);
        }
    }

    private KinectGrabbingHand.HandInfo GetGoodHand()
    {
        return whichHand == SideType.Right ? hands.handRight : hands.handLeft;
    }
    


    private bool IsTheGoodHand(ref KinectGrabbingHand.HandInfo handInfo)
    {
        if (whichHand == SideType.Left && handInfo.side == KinectGrabbingHand.HandInfo.Side.Left) return true;
        if (whichHand == SideType.Right && handInfo.side == KinectGrabbingHand.HandInfo.Side.Right) return true;
        return false;
    }
    
}
