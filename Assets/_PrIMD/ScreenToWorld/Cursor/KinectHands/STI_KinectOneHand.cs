﻿using UnityEngine;
using System.Collections;

public class STI_KinectOneHand : MonoBehaviour , ICursorInfoSource {


    public KinectGrabbingHand hands;
    public enum SideType { Left, Right }
    public SideType whichHand = SideType.Right;

   

    public Vector3 ScreenPosition
    {
        get
        {
            if (hands == null) return Vector3.zero;
            KinectGrabbingHand.HandInfo hand = GetGoodHand();
            if (!hand.hasHandUp) return Vector3.zero;
            return hands.GetCustomScreenPosition(ref hand, null, true, 0);
        }
    }

    public bool IsPressed 
    {
        get
        {
            if (hands == null) return false;
            KinectGrabbingHand.HandInfo hand = GetGoodHand();
            if (!hand.hasHandUp) return false;
            return hand.isUsed;
        }
    }

    private KinectGrabbingHand.HandInfo GetGoodHand()
    {
        return whichHand == SideType.Right ? hands.handRight : hands.handLeft;
    }


    public Vector3 GetScreenPosition()
    {
        return ScreenPosition;
    }

    public bool IsCursorActive()
    {
        return IsPressed;
    }
}
