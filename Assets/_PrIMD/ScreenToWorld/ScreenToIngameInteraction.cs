﻿/*Coded by PrIMD (primd42@gmail.com) for Kraken RealTime (http://krakenrealtime.com/)*/

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;


/// <summary>
/// This class give the possiblity to interact with GUI 4.6 
/// </summary>
public  class ScreenToIngameInteraction : MonoBehaviour {


    private bool isAllowToBeActive = true;
    public void SetActive(bool onOff)
    {
        if(!onOff)
           Deactivate();
        isAllowToBeActive = onOff;
    }
    public void Deactivate() {

        if (isButtonDown)
            Up();
        Exit();
    }

    public string deviceName = "Kinect";
    public enum SubmitWhen { Down, Up, Enter, StayHover, StayActive, Exit, None }
    public SubmitWhen submitWhen = SubmitWhen.Down;
    public float defaultStayOver = 2.5f;
    public float defaultStayActive = 1f;

    public bool submitOnlyOnce;
    private int submitCount;
    public float submitCooldown = 2f;
    private float submitCooldownCount = 0;
   

    public bool IsOnButton { get; private set; }
    //Private later;
    public bool isHandActive;
    //Private later;
    public bool isButtonDown;
    //Private later;
    public float hoverSince;
    //Private later;
    public float activeSince;

    public bool withButtonEvent=true;
    public bool withBroadcast;
    public bool withListener;
    public bool allowButtonCustumTimeForStay=true;

    //Private later;
    #region Button targeted state
    public Button _selectedButton;
    public Button SelectedButton
    {
        get { return _selectedButton; }
        set
        {
            if (_selectedButton != value)
                ButtonChangeDetected(_selectedButton, value);
            _selectedButton = value;
        }
    }

    public delegate void CursorEvent(ScreenToIngameInteraction screenInter);
    public CursorEvent onMouseEnter;
    public CursorEvent onMouseDown;
    public CursorEvent onMouseSubmit;
    public CursorEvent onMouseUp;
    public CursorEvent onMouseExit;

    public delegate void CursorStayEvent(ScreenToIngameInteraction screenInter, float timeLeft, float initialTime);
    public CursorStayEvent onMouseHover;
    public CursorStayEvent onMouseStay;

    public ICursorInfoSource FirstCursorOver { get; private set; }
    public ICursorInfoSource [] Cursors { get { return cursorsObserved.ToArray(); } }
    private List<ICursorInfoSource> cursorsObserved=new List<ICursorInfoSource>();
    public GameObject[] cursorsToObserve;
    public void AddCursors(ICursorInfoSource cursor) {
        if (cursor != null)
            cursorsObserved.Add(cursor);
    }
    public void RemoveCursors(ICursorInfoSource cursor)
    {
        if (cursor != null)
            cursorsObserved.Remove(cursor);
    }


    private void ButtonChangeDetected(Button oldButt, Button newButt)
    {
        if (oldButt != null)
        {
            SwitchOff(oldButt);
        }
        if (newButt != null)
            SwitchOn(newButt);

        if (oldButt == null)
            IsOnButton = true;
        if (newButt == null)
            IsOnButton = false;

        hoverSince = 0f;
        activeSince = 0f;
    }

    private void SwitchOn(Button newButt)
    {
        Enter(newButt);
        Hover(newButt);
    }

    private void SwitchOff(Button oldButt)
    {
        Exit(oldButt);
    } 
    #endregion
    #region Hand management
    
  
    public Vector3 ScreenPosition
    {get;set;}


    private void Press()
    {
        SetPressState(true);
    }
    private void Release()
    {
        SetPressState(false);
    }
    private void SetPressState(bool isPress)
    {

        isHandActive = isPress;

    }

   

    #endregion
    #region Update and method linked

    void Start() {

        foreach (GameObject cursorGamo in cursorsToObserve)
        { 
            ICursorInfoSource source = cursorGamo.GetComponent(typeof(ICursorInfoSource)) as ICursorInfoSource;
            AddCursors(source);
        }
    }
    void Update()
    {

        DoTheHandsAreOverAnyButton();

        if (submitCooldownCount > 0f)
        {
            submitCooldownCount = Mathf.Clamp(submitCooldownCount - Time.deltaTime, 0f, float.MaxValue);
        }
        CheckAndSendButtonState();
    }

    //Var in aim to do not instanciate it every time
    private List<ICursorInfoSource> cursorsHover = new List<ICursorInfoSource>(); 
    private bool DoTheHandsAreOverAnyButton()
    {
        //Set Screen Position
        PointerEventData pe = new PointerEventData(EventSystem.current);
        //If not button is selected,  look for one cursor that is over.
        if (SelectedButton == null) { 
            Button butt;
            
            butt = GetFirstButtonWithCursorHover(ref pe);
            SelectedButton = butt;
        }

        if(SelectedButton!=null){
            GetAllCursorsPointingAtButton(SelectedButton, ref cursorsHover, ref pe);

            if (cursorsHover.Count <= 0) {
                //Unselect the current object if none is over it
                SetPressState(false);
                SelectedButton = null; 
            }
            else
            {
                SetPressState(IsAnyCursorsPressing(ref cursorsHover));
            }
        }
        
        return SelectedButton == null;
    }

    private bool IsAnyCursorsPressing(ref List<ICursorInfoSource> cursorsHoverList)
    {
        bool isAnyPressing = false;
        foreach (ICursorInfoSource cursor in cursorsHoverList)
        {
            if (cursor.IsCursorActive())
            {
                isAnyPressing = true;
                break;
            }
        }
        return isAnyPressing;
    }

    private void GetAllCursorsPointingAtButton(Button button, ref List<ICursorInfoSource> cursorHoverList, ref PointerEventData pe)
    {
        Button butt;
        cursorHoverList.Clear();
        foreach (ICursorInfoSource source in cursorsObserved)
        {
            //NB: Deal with first one, it could have several buttun one beside an other
            butt = GetButtonByRaycast(source.GetScreenPosition(), ref pe);
            if (butt == button)
                cursorHoverList.Add(source);
        }
    }

    private Button GetFirstButtonWithCursorHover(ref PointerEventData pe )
    {
        Button butt=null;
        FirstCursorOver = null;
        foreach (ICursorInfoSource source in cursorsObserved)
        {
            butt = GetButtonByRaycast(source.GetScreenPosition(), ref pe);
            if (butt != null) {
                FirstCursorOver = source;
                break; }

        }

        return butt;
    }

    private Button GetButtonByRaycast(Vector3 ScreenPosition, ref PointerEventData pointEvent)
    {

        pointEvent.position = ScreenPosition;
        //User raycast
        List<RaycastResult> hits = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointEvent, hits);
        
        //Explore the result
        Button butt = null;
        foreach (RaycastResult h in hits)
        {
            GameObject gamoHit = h.gameObject;
            butt = gamoHit.GetComponent<Button>() as Button;
            if (butt != null) break;
        }
        return butt;
    }
    private void CheckAndSendButtonState()
    {
        if (SelectedButton == null) return;
        if (isHandActive && !isButtonDown)
        {
            isButtonDown = true;
            Down();

        }
        else if (!isHandActive && isButtonDown)
        {

            isButtonDown = false;
            Up();
        }
        
        hoverSince += Time.deltaTime;
        Hover();
        if (isButtonDown)
        {
            activeSince += Time.deltaTime;
            Stay();
        }
        
    }
    
    #endregion
    #region Enter, Hover, Down, Stay, Up ,Exit -?-> Submit + Broadcast
    private void Stay(Button button = null, PointerEventData pointer = null)
    {
        if (!isAllowToBeActive) return;
        if (!CheckAndComplete(ref pointer, ref button)) return;
        float timeToBeActive = GetTimeNeededToBeActive(SubmitWhen.StayActive, ref button);
        if (CheckIfSubmitMustBeCall(SubmitWhen.StayActive))
        {
            if (activeSince > timeToBeActive)
                Submit(button, pointer);
        }

        if (onMouseStay != null)
            onMouseStay(this, timeToBeActive - hoverSince, timeToBeActive);
        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Stay", timeToBeActive-activeSince, SendMessageOptions.DontRequireReceiver);
        } 
        if (withListener)
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if (listener != null)
                listener.OnScreenInteractionStay(this, deviceName, timeToBeActive - hoverSince, timeToBeActive);

        }
        
    }
    private void Hover(Button button = null, PointerEventData pointer = null)
    {
        if (!isAllowToBeActive) return;
        if (!CheckAndComplete(ref pointer, ref button)) return;

        float timeToBeActive = GetTimeNeededToBeActive(SubmitWhen.StayHover, ref button);
        if (CheckIfSubmitMustBeCall(SubmitWhen.StayHover))
        {
            if (hoverSince > timeToBeActive)
                Submit(button, pointer);
        }
        if (onMouseStay != null)
            onMouseHover(this, timeToBeActive - hoverSince, timeToBeActive);

        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Over", timeToBeActive-hoverSince, SendMessageOptions.DontRequireReceiver);
        }
        if (withListener) 
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if(listener!=null)
                listener.OnScreenInteractionOver(this, deviceName, timeToBeActive - hoverSince, timeToBeActive);

        }
    }

    private void Enter(Button button = null, PointerEventData pointer = null)
    {
        if (!isAllowToBeActive) return;
        if (!CheckAndComplete(ref pointer, ref button)) return;

        if (CheckIfSubmitMustBeCall(SubmitWhen.Enter))
            Submit(button, pointer);

        if (withButtonEvent)
        ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        if (onMouseEnter != null)
            onMouseEnter(this);

        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Enter", SendMessageOptions.DontRequireReceiver);
        }
        if (withListener)
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if (listener != null)
                listener.OnScreenInteractionEnter(this, deviceName);

        }
    }

    private void Down(Button button = null, PointerEventData pointer = null)
    {
        if (!CheckAndComplete(ref pointer, ref button)) return;
        if (CheckIfSubmitMustBeCall(SubmitWhen.Down))
            Submit(button, pointer);
        if (onMouseDown != null)
            onMouseDown(this);

        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Down", SendMessageOptions.DontRequireReceiver);
        }if (withListener)
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if (listener != null)
                listener.OnScreenInteractionDown(this, deviceName);

        }
    }

    private void Up(Button button = null, PointerEventData pointer = null)
    {
        if (!isAllowToBeActive) return;
        if (!CheckAndComplete(ref pointer, ref button)) return;
        if (CheckIfSubmitMustBeCall(SubmitWhen.Up))
            Submit(button, pointer);
        if (withButtonEvent)
        {
            ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerUpHandler);
            ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.selectHandler);
        }
        if (onMouseUp != null)
            onMouseUp(this);

        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Up", SendMessageOptions.DontRequireReceiver);
        }if (withListener)
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if (listener != null)
                listener.OnScreenInteractionUp(this, deviceName);

        }
    }

    private void Exit(Button button = null, PointerEventData pointer = null)
    {
        if (!isAllowToBeActive) return;
        if (!CheckAndComplete(ref pointer, ref button)) return;
        if (CheckIfSubmitMustBeCall(SubmitWhen.Exit))
            Submit(button, pointer);

        if (withButtonEvent) { 
        ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerExitHandler);
        ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.deselectHandler);
        }
        if (onMouseExit != null)
            onMouseExit(this);

        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Exit", SendMessageOptions.DontRequireReceiver);
        }if (withListener)
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if (listener != null)
                listener.OnScreenInteractionExit(this, deviceName);

        }

        submitCount = 0;
    }

    private void Submit(Button button = null, PointerEventData pointer = null)
    {

        if (!isAllowToBeActive) return;

        if (submitOnlyOnce && submitCount > 0) return;
        if (submitCooldownCount > 0) return;
        if (!CheckAndComplete(ref pointer, ref button)) return;

        if (withButtonEvent)
            ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.submitHandler);
        if (onMouseSubmit != null)
            onMouseSubmit(this);

        if (withBroadcast)
        {
            button.BroadcastMessage("On"+deviceName+"Submit", SendMessageOptions.DontRequireReceiver);
        }if (withListener)
        {
            IScreenInteractionListener listener = button.GetComponent(typeof(IScreenInteractionListener)) as IScreenInteractionListener;
            if (listener != null)
                listener.OnScreenInteractionSubmit(this, deviceName);

        }
        submitCooldownCount = submitCooldown;
        submitCount++;
    } 
    #endregion
    #region Procedure Methode
    private bool CheckIfSubmitMustBeCall(SubmitWhen submitZone)
    {
        bool submit = false;
        switch (submitZone)
        {
            case SubmitWhen.Down: submit = submitWhen == SubmitWhen.Down; break;
            case SubmitWhen.Up: submit = submitWhen == SubmitWhen.Up; break;
            case SubmitWhen.Enter: submit = submitWhen == SubmitWhen.Enter; break;
            case SubmitWhen.StayHover: submit = submitWhen == SubmitWhen.StayHover; break;
            case SubmitWhen.StayActive: submit = submitWhen == SubmitWhen.StayActive; break;
            case SubmitWhen.Exit: submit = submitWhen == SubmitWhen.Exit; break;
        }
        return submit;
    }
    private bool CheckAndComplete(ref PointerEventData pointer, ref Button button)
    {
        if (pointer == null)
        {
            pointer = new PointerEventData(EventSystem.current);
        }
        if (button == null)
        {
            button = SelectedButton;
        }
        return pointer != null && button != null;
    }
    private float GetTimeNeededToBeActive(SubmitWhen submitWhen, ref Button button)
    {
        
        float time = defaultStayOver;
        if (submitWhen == SubmitWhen.StayActive)
            time = defaultStayActive;

        if (!allowButtonCustumTimeForStay) return time;

        IScreenInteractionCustomStay customTime = button.gameObject.GetComponent(typeof(IScreenInteractionCustomStay)) as IScreenInteractionCustomStay;
        if (customTime != null)
        {
            if (submitWhen == SubmitWhen.StayActive)
                time = customTime.GetTimeToBeSubmitOnStay();
            if (submitWhen == SubmitWhen.StayHover)
                time = customTime.GetTimeToBeSubmitOnOver();
        }
        return time;
    }
    
    #endregion

}

public interface ICursorInfoSource
{
    Vector3 GetScreenPosition();
    bool IsCursorActive();
}
public interface IScreenInteractionCustomStay
{
    float GetTimeToBeSubmitOnStay();
    float GetTimeToBeSubmitOnOver();

}

public interface IScreenInteractionListener 
{
    void OnScreenInteractionSubmit(ScreenToIngameInteraction from, string device);
    void OnScreenInteractionEnter(ScreenToIngameInteraction from, string device);
    void OnScreenInteractionDown(ScreenToIngameInteraction from, string device);
    void OnScreenInteractionOver(ScreenToIngameInteraction from, string device, float timeLeft, float initalTime);
    void OnScreenInteractionStay(ScreenToIngameInteraction from, string device, float timeLeft, float initalTime);
    void OnScreenInteractionUp(ScreenToIngameInteraction from, string device);
    void OnScreenInteractionExit(ScreenToIngameInteraction from, string device);

}
