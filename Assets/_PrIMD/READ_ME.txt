All code in this folder is under Beerware License: 
http://www.primd.be/beerware/


/* --------------------------BEER-WARE LICENSE----------------
 * Str�e Eloi (PrIMD42@gmail.com) wrote this file.
 * ======  =======   PERSONAL USE   ====== ======
 * As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you could buy me a beer in return, 
 * ====== ========  COMMERCIAL USE   ====== ====
 * Please refer to personal use license.
 * Plus, send me an email with :
 * Name, Project name,  grade (1-10) and your opinions
 *_______________   LINK   _____________________
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */