﻿using UnityEngine;
using System.Collections;
using System;

public class TrackedKinectBasicHandsInfo : BasicUnityTrackingPoint
{
    public KinectHandsBasicInfo kinectHandInfo;
    public enum TrackedElement {HandLeft,HandRight,  ShoulderLeft, ShoulderRight, Neck}
    public TrackedElement trackedElement;



    private Quaternion GetLocalRotation()
    {
        throw new NotImplementedException();
    }

    private Quaternion GetRotation()
    {
        switch (trackedElement)
        {
            case TrackedElement.HandLeft:
                //récupérer la direction
                //Transformer en rotation
                throw new NotImplementedException();
            case TrackedElement.HandRight:
                //récupérer la direction
                //Transformer en rotation
                throw new NotImplementedException();
            case TrackedElement.ShoulderLeft:
            case TrackedElement.ShoulderRight:
            case TrackedElement.Neck:
            default:
                return kinectHandInfo.KinQuat_Neck_LastSafe;
        }
    }

    private Vector3 GetLocalPosition()
    {
        //NOT TESTED
        Vector3 pos = GetPosition();
        Quaternion tmpQ = Quaternion.identity;
        RecenterPoint(kinectHandInfo.KinPos_Neck_LastSafe, kinectHandInfo.KinQuat_Neck_LastSafe, ref pos, ref tmpQ);
        return pos;
    }

    private Vector3 GetPosition()
    {
        switch (trackedElement)
        {
            case TrackedElement.HandLeft:
               return  kinectHandInfo.KinPos_handLeft_LastSafe;
            case TrackedElement.HandRight:
               return kinectHandInfo.KinPos_handRight_LastSafe;
            case TrackedElement.ShoulderLeft:
               return kinectHandInfo.KinPos_shoulderLeft_LastSafe;
            case TrackedElement.ShoulderRight:
               return kinectHandInfo.KinPos_shoulderRight_LastSafe;
            case TrackedElement.Neck:
            default:
                return kinectHandInfo.KinPos_Neck_LastSafe;
        }
    }


    private void RecenterPoint(Vector3 rootPosition, Quaternion rootRotation, ref Vector3 pos, ref Quaternion rot)
    {
        Vector3 initPos = pos;
        Quaternion r =  Quaternion.Inverse(rootRotation);
        pos = pos - rootPosition;
        pos = r * pos;
        rot = rot * r;
    }
}
