﻿using UnityEngine;
using System.Collections;

public interface IOffsetResetable  {

    void ResetOffset();
}
