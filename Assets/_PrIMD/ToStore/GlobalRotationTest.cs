﻿using UnityEngine;
using System.Collections;

public class GlobalRotationTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0)) 
        {
            transform.rotation = Quaternion.FromToRotation(Vector3.up, Vector3.right) * transform.rotation;
        }
	}
}
