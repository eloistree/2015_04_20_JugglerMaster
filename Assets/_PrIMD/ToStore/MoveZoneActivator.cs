﻿
/* --------------------------BEER-WARE LICENSE----------------
 * Strée Eloi (PrIMD42@gmail.com) wrote this file.
 * ======  =======   PERSONAL USE   ====== ======
 * As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you could buy me a beer in return, 
 * ====== ========  COMMERCIAL USE   ====== ====
 * Please refer to personal use license.
 * Plus, send me an email with :
 * Name, Project name,  grade (1-10) and your opinions
 *_______________   LINK   _____________________
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */
using UnityEngine;
using System.Collections.Generic;
using System;
public class MoveZoneActivator : MonoBehaviour {


	public Transform observedTransform;
	public Transform rootRef;
	public Vector2 positionAdjustment;
	public float x;
	public float y;
	public bool isActive;
	
	
	public bool observeLateralAxe = true;
	public float minLateralDistance=50;
	public float maxLateralDistance=100;
	
	
	
	public bool observeFrontAxe=true;
	public float minFrontDistance=50;
	public float maxFrontDistance=100;
	
	
	public bool enterLeft;
	public bool enterRight;
	public bool enterFront;
	public bool enterBack;
	
	public bool isMoving;
	public bool isOutOfLateralZone;
	public bool isOutOfFrontZone;
	
	public bool withDebugDraw=true;
	
	public bool withListner=true;
	public readonly Listner  listners;
	public bool withEvents = true;
	public readonly Events  events;

	public bool withSimulationData;
	public Vector3 simulationData;

	/**In the situation where the root is fixed, it can be usefull to readjust by set the root at the object followed position*/
	public void RootAutoAdjustement(){
		//not tested
		Debug.Log ("Auto Readjustment");
		positionAdjustment = Vector3.zero;
		Vector3 root = rootRef.transform.position;
		Vector3 observed = observedTransform.transform.position;
//		rootRef.transform.position =observedTransform.transform.position;
		Vector3 adjTmp = observed-root;
		//Vector3 adjTmp = - root + observed;
		positionAdjustment.x = adjTmp.x;
		positionAdjustment.y = adjTmp.z;
	}
	
	public MoveZoneActivator()
	{
		listners = new Listner ();
		events = new Events (this);
	}
	
	protected void Start()
	{
		if (observedTransform == null)
			Destroy (this);
		if (observedTransform != null) {

		}
	
	}
	
	public void SetActive(bool onOff)
	{
		isActive = onOff;
	}
	public bool IsActive(){
		return isActive;
	}
	
	protected void Update()
	{
		
		if(isActive && (withListner || withEvents))
		{
			CheckData();
		}
		
	}

	public void CheckData ()
	{
		if (withSimulationData) {
				x = simulationData.x;
				y = simulationData.y;
		} else {


			Vector3 rootAdjustedPosition=  rootRef.position;
			rootAdjustedPosition.x += positionAdjustment.x;
			rootAdjustedPosition.z += positionAdjustment.y;
			Vector3 relocatedObsPoint = Utility.BasedOnPosition.Relocated(observedTransform.position,rootAdjustedPosition, rootRef.rotation, false, true, false);

			//Debug.Log("r: "+relocatedObsPoint);

//			if(withDebugDraw)
//			{
//				Debug.DrawLine( rootRef.position, rootRef.position+(relocatedObsPoint*20), Color.yellow);
//			}
			
			//x= relocatedObsPoint.x-positionAdjustment.x;
			//y= relocatedObsPoint.z-positionAdjustment.y;
			x= relocatedObsPoint.x;
			y= relocatedObsPoint.z;
		}

		bool IsLateralInZone= x > minLateralDistance || x < -minLateralDistance ;
		bool IsFrontInZone= y > minFrontDistance || y < -minFrontDistance;

		isOutOfLateralZone= x > maxLateralDistance ||  x < - maxLateralDistance;
		isOutOfFrontZone= y > maxFrontDistance ||  y < - maxFrontDistance;

		float pourcentLateral	= (Mathf.Abs(x)-minLateralDistance) / (maxLateralDistance -minLateralDistance);
		float pourcentFront 	= (Mathf.Abs(y)-minFrontDistance) / (maxFrontDistance -minFrontDistance);
		
		if (pourcentFront > 1) {
			pourcentFront = 1f;
			isOutOfFrontZone=true;
		}
		if (pourcentLateral > 1) {
			pourcentLateral = 1f;
			isOutOfLateralZone=true;
		
		}

		if(IsLateralInZone|| IsFrontInZone )
		{
			if(!isMoving)
			{	
//				Debug.Log("START ------ Move ------->");
				if(withEvents)
					events.NotifyStartMoving(GetTime());
				isMoving = true;
			}


			if (x > minLateralDistance) {
				if(withListner)
					listners.OnRight(pourcentLateral, Time.deltaTime);
				if(withEvents && enterRight) 
				{
					enterRight=true;
					events.NotifyRightMoving(GetTime());
				}
			}
			else if (x < -minLateralDistance) {
				if(withListner)
					listners.OnLeft(pourcentLateral, Time.deltaTime);
				if(withEvents && enterLeft) 
				{
					enterLeft=true;
					events.NotifyLeftMoving(GetTime());
				}
			} 
			
			if (y > minFrontDistance) {
				if(withListner)
					listners.OnFront(pourcentFront, Time.deltaTime);
				if(withEvents && enterFront) 
				{
					enterFront=true;
					events.NotifyFrontMoving(GetTime());
				}

			}
			else if (y < -minFrontDistance) {
				if(withListner)
					listners.OnBack(pourcentFront, Time.deltaTime);
				if(withEvents && enterBack) 
				{
					enterBack=true;
					events.NotifyBackMoving(GetTime());
				}
			} 

			
		}
		else
		{
			isOutOfFrontZone =false;
			isOutOfLateralZone =false;

			enterBack=false;
			enterFront=false;
			enterRight=false;
			enterLeft=false;
			// notify that the view is in the good zone and stop rotating
			if(isMoving)  
			{	
	//			Debug.Log("<----------Move---------  END");
				if(withEvents)
				events.NotifyEndMoving(GetTime());
				isMoving = false;
			}

		}
		////TEST 


	}
	
	public float GetTime(){return Time.timeSinceLevelLoad;}
	
	public class Listner
	{
		
		public List<MoveZoneListner> listners = new List<MoveZoneListner>();
		
		public void AddListner(MoveZoneListner listner){
			if(listner!=null) listners.Add (listner);		
		}
		
		public void Clear(){
			listners.Clear ();
		}

		
		public void OnLeft(float pourcent, float deltaTime){
			foreach (MoveZoneListner l in listners) {
				l.OnMoveLeft(pourcent, deltaTime);			
			}
		}
		
		public void OnRight(float pourcent, float deltaTime){
			foreach (MoveZoneListner l in listners) {
				l.OnMoveRight(pourcent, deltaTime);			
			}
		}
		
		public void OnFront(float pourcent, float deltaTime){
			foreach (MoveZoneListner l in listners) {
				l.OnMoveFront(pourcent, deltaTime);			
			}
		}
		
		public void OnBack(float pourcent, float deltaTime){
			foreach (MoveZoneListner l in listners) {
				l.OnMoveBack(pourcent, deltaTime);			
			}
		}
		
		
		
	}
	
	public class Events
	{
		
		
		public EventHandler<StartMovingArgs> onStartMoving;
		public EventHandler<EndMovingArgs> onEndMoving;
		
		public EventHandler<StartMovingLeftArgs> onMoveLeft;
		public EventHandler<StartMovingRightArgs> onMoveRight;
		public EventHandler<StartMovingFrontArgs> onMoveFront;
		public EventHandler<StartMovingBackArgs> onMoveBack;
		
		
		MoveZoneActivator parent;
		
		public Events (MoveZoneActivator parent)
		{
			this.parent = parent;
		}
		
		
		public void NotifyStartMoving(float time){
			if (onStartMoving!=null) {
				onStartMoving(parent, new StartMovingArgs(time, parent));
			}
		}
		
		public void NotifyEndMoving(float time){
			if (onEndMoving!=null) {
				onEndMoving(parent, new EndMovingArgs(time, parent));
			}
			
		}
		
		
		public void NotifyLeftMoving(float time){
			if (onMoveLeft!=null) {
				onMoveLeft(parent, new StartMovingLeftArgs(time, parent));
			}
			
		}
		
		public void NotifyRightMoving(float time){
			if (onMoveRight!=null) {
				onMoveRight(parent, new StartMovingRightArgs(time, parent));
			}
			
		}
		
		public void NotifyFrontMoving(float time){
			if (onMoveFront!=null) {
				onMoveFront(parent, new StartMovingFrontArgs(time, parent));
			}
			
		}
		
		public void NotifyBackMoving(float time){
			if (onMoveBack!=null) {
				onMoveBack(parent, new StartMovingBackArgs(time, parent));
			}
			
		}
		
		
	}
	
	
}
public interface MoveZoneListner
{
	void OnMoveLeft (float pourcent, float deltaTime);
	void OnMoveRight (float pourcent, float deltaTime);
	void OnMoveFront (float pourcent, float deltaTime);
	void OnMoveBack (float pourcent, float deltaTime);
}
public class MoveZoneEventArgs  : EventArgs
{
	public float when;
	public MoveZoneActivator moveZone;
	
	public MoveZoneEventArgs (float when, MoveZoneActivator moveZone)
	{
		this.when = when;
		this.moveZone = moveZone;
	}
	
	
}

public class StartMovingArgs : MoveZoneEventArgs
{
	public StartMovingArgs (float when, MoveZoneActivator rotationZone):  base(when, rotationZone){}
}

public class EndMovingArgs : MoveZoneEventArgs
{
	public EndMovingArgs (float when, MoveZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartMovingLeftArgs : MoveZoneEventArgs
{
	public StartMovingLeftArgs (float when, MoveZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartMovingRightArgs : MoveZoneEventArgs
{
	public StartMovingRightArgs (float when, MoveZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartMovingFrontArgs : MoveZoneEventArgs
{
	public StartMovingFrontArgs (float when, MoveZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartMovingBackArgs : MoveZoneEventArgs
{
	public StartMovingBackArgs (float when, MoveZoneActivator rotationZone):  base(when, rotationZone){}}