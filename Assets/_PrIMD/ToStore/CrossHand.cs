﻿using UnityEngine;
using System.Collections;

public class CrossHand : MonoBehaviour
{
    public KinectHandsBasicInfo kinectHandInfo;
    public float minHandsDistance = 0.2f;

    public bool withHandsHeightFromNeck;
    public float handsHeight = -0.3f;
    public float handsHeightRange = 0.2f;

    public float checkEverySecond = 0.25f;
    public float countDown=0;
       
    private bool _isCrossingHand;
    public delegate void OnHandCrossDetected();
    public OnHandCrossDetected onCrossHand;
    public bool IsCrossingHand
    {
        get { return _isCrossingHand; }
        set {
        if (!_isCrossingHand && value)
            if (onCrossHand != null)
                onCrossHand();
        _isCrossingHand = value;
        }
    }

	void Update () {
        if (kinectHandInfo == null) 
            return;
        countDown -= Time.deltaTime;
        if (countDown < 0f) {
            countDown = checkEverySecond;

               IsCrossingHand =HandRightAtLeft( minHandsDistance);
        }
	}

    private bool HandRightAtLeft(float distanceBetween)
    {
        if (kinectHandInfo == null) return false;
        Vector3 handLeft = kinectHandInfo.GetPosition(KinectHandsBasicInfo.TrackedElement.HandLeft, true);
        Vector3 handRight = kinectHandInfo.GetPosition(KinectHandsBasicInfo.TrackedElement.HandRight, true);
        Vector3 neck = kinectHandInfo.GetPosition(KinectHandsBasicInfo.TrackedElement.Neck, true);

        if (handRight.x < handLeft.x)
            if (Vector3.Distance(handRight, handLeft) > distanceBetween)
                if (handRight.x < neck.x && handLeft.x > neck.x)
                {
                    if (!withHandsHeightFromNeck)
                        return true;
                    else {
                        float handsY = (handLeft.y + handRight.y)/2f;
                        float neckY = neck.y;
                        float y = handsY - neckY;
                      //  Debug.Log(string.Format("Hand Y {0}   Neck Y {1}     {2} <  Z {3}  < {4}", handsY, neckY, handsHeight - handsHeightRange, y, handsHeight + handsHeightRange));
                        if (y > handsHeight - handsHeightRange && y < handsHeight + handsHeightRange)
                            return true;
                    }
                }
                
        return  false ;
    }
}
