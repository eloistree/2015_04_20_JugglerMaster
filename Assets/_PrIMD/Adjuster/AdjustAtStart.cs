﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdjustAtStart : UnityInspectorAdjustement {


    public Transform root;
    public void Start()
    {
           adjuster.ApplyAdjustements(root);
    }
}
