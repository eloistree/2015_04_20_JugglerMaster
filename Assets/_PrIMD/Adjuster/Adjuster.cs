﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
[System.Serializable]
public class Adjuster 
{

    public List<CommonAdjustement> toApply;
    private bool startAdded;
    private List<Adjustement> adjustements = new List<Adjustement>();

    public void ApplyAdjustements(Transform directionalRoot)
    {
        if (directionalRoot == null) throw new ArgumentNullException();
        Vector3 position = directionalRoot.position;
        Quaternion rotation = directionalRoot.rotation;

        ApplyAdjustements(ref position, ref rotation);

        directionalRoot.position = position;
        directionalRoot.rotation = rotation;
    }
    internal void ApplyAdjustements(ref Vector3 position, ref Quaternion rotation)
    {
        if (!startAdded) {
            AddStartValue();
        }
        foreach (Adjustement adj in adjustements)
        {
            if (adj != null)
            {
                adj.ApplyAt(ref position, ref rotation);
            }
        }
    }

    private void AddStartValue()
    {
        foreach (CommonAdjustement adj in toApply)
            AddAdjustement(adj);
        startAdded = true;
    }

    public Adjustement this[int i]
    {
        get { return adjustements[i]; }
    }
    public void AddAdjustement(Adjustement adjustement)
    {
        adjustements.Add(adjustement);
    }

    public void AddAjustementAt(int index,Adjustement adjustement  )
    {
        if (index >= adjustements.Count || index<0)
            adjustements.Add(adjustement);
        else 
            adjustements.Insert(index, adjustement);
    
    }


    public List<Adjustement> GetAdjustements()
    {
        return adjustements;
    }

 
}

public interface Adjustement {

    void ApplyAt(ref Vector3 position, ref Quaternion orientation);
}

[System.Serializable]
public abstract class AdjustementAbst : Adjustement {
    public abstract void ApplyAt(ref Vector3 position, ref Quaternion orientation);
}

[System.Serializable]
public class CommonAdjustement : AdjustementAbst
{
    public Vector3 value;
    public enum AdjustementType { Translate, EuleurRotation, AxialRotation, EulerRotationGlobal, AxialRotationGlobal, TranslateWithOrientation, MultiplyPosition}
    public AdjustementType adjustement;

    public override void ApplyAt(ref Vector3 where, ref Quaternion orientation)
    {
        ApplyAdjustement(ref where, ref orientation, adjustement, value);
    }

    public static void ApplyAdjustement(ref Vector3 where, ref Quaternion orientation, AdjustementType adjustement, Vector3 value) {
        switch (adjustement)
        {
            case AdjustementType.Translate: ApplyTranslate(ref where, ref orientation, value, false); break;
            case AdjustementType.TranslateWithOrientation: ApplyTranslate(ref where, ref orientation, value,true); break;
            case AdjustementType.EuleurRotation: ApplyEuleurRotation(ref where, ref orientation, value); break;
            case AdjustementType.AxialRotation: ApplyAxialRotation(ref where, ref orientation, value); break;
            case AdjustementType.EulerRotationGlobal: ApplyEuleurRotation(ref where, ref orientation, value, true); break;
            case AdjustementType.AxialRotationGlobal: ApplyAxialRotation(ref where, ref orientation, value, true); break;
            case AdjustementType.MultiplyPosition: ApplyMultiplicationOnPosition(ref where, ref orientation, value); break;
        }
    }

    private static void ApplyMultiplicationOnPosition(ref Vector3 where, ref Quaternion orientation, Vector3 value)
    {
        where.x *= value.x;
        where.y *= value.y;
        where.z *= value.z;
    }

    private static void ApplyAxialRotation(ref Vector3 where, ref Quaternion orientation, Vector3 value,bool global=false)
    {
        
        Vector3 tmp;
        if (value.x != 0f)
        {
            tmp = Vector3.zero;
            tmp.x = value.x;
            if (global)
                orientation = Quaternion.Euler(tmp) * orientation;
            else 
                orientation *= Quaternion.Euler(tmp);
        }
        if (value.y != 0f)
        {
            tmp = Vector3.zero;
            tmp.y = value.y;
            if (global)
                orientation = Quaternion.Euler(tmp) * orientation;
            else    
                orientation *= Quaternion.Euler(tmp);
        }
        if (value.z != 0f)
        {
            tmp = Vector3.zero;
            tmp.z = value.z;
            if (global)
                orientation = Quaternion.Euler(tmp) * orientation;
            else 
                orientation *= Quaternion.Euler(tmp);
        }
    }


    private static void ApplyEuleurRotation(ref Vector3 where, ref Quaternion orientation, Vector3 value, bool global = false)
    {
        if (global)
            orientation = Quaternion.Euler(value) * orientation;
        else 
            orientation *= Quaternion.Euler(value);
    }

    private static void ApplyTranslate(ref Vector3 where, ref Quaternion orientation, Vector3 value, bool withOrientation)
    {
        if (withOrientation)
            where += orientation * value;
        else where += value;
    }
}
