﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnityInspectorAdjustement : MonoBehaviour {

    public string reminderDescription;
    public Adjuster adjuster = new Adjuster();
    public List<CommonAdjustement> inspectorCommonAdjustements;

    public void Awake()
    {
        foreach (Adjustement adj in inspectorCommonAdjustements)

            adjuster.AddAdjustement(adj);
    }

}
