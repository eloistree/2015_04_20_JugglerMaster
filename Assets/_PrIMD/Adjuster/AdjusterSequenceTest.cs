﻿using UnityEngine;
using System.Collections;

public class AdjusterSequenceTest : AdjustAtStart
{

    public float timeBetween=1f;
    
    public  void Start()
    {
       StartCoroutine( ApplyAdjustements(root));
    }

    public void Update() 
    {
        root.position = Vector3.Lerp(root.position, position, Time.deltaTime);
        root.rotation = Quaternion.Lerp(root.rotation, rotation, Time.deltaTime);

    
    }
    Vector3 position ;
    Quaternion rotation ;

    public IEnumerator ApplyAdjustements(Transform directionalRoot)
    {
        if (directionalRoot == null)
        {
            Debug.LogWarning("ApplyAdjustements requiere a Transform to apply !", this.gameObject);
            yield break;
        }
         position = directionalRoot.position;
         rotation = directionalRoot.rotation;

        foreach (Adjustement adj in adjuster.GetAdjustements())
        {
            if (adj != null)
            {
                adj.ApplyAt(ref position, ref rotation);
                yield return new WaitForSeconds(timeBetween);
            }
        }

        

    }
}
