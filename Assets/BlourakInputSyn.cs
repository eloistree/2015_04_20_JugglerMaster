﻿using UnityEngine;
using System.Collections;
using Rewired;

public class BlourakInputSyn : MonoBehaviour {

    public string idName="Player1";
    public Player player;

    public string onGroundCatName = "PlayerOnGround";
    public string  inAirCatName = "PlayerInAir";

    public Checker groundChecker;
    public Checker wallChecker;

    public Vector2 _direction;
    public Vector2 MoveDirection { get { return _direction; } private set { _direction = value; } }

    public bool _hasGround;
    public bool HasGround
    {
        get
        {
            return _hasGround;
        }
        set
        {
            bool oldVal = _hasGround;

            _hasGround = value;
            if (oldVal != value)
            {
                SetEnabledStateOnMapsInCategory(player, onGroundCatName, _hasGround);
                SetEnabledStateOnMapsInCategory(player, inAirCatName, !_hasGround);
            }
        }
    
    }//{get; private set;}

    public bool HasWall;//{ get; private set; }
    public bool Running;//{ get; private set; }
    public bool Jump;//{ get; private set; }
    public bool AirJump;//{ get; private set; }
    public bool Fire;//{ get; private set; }
    public bool Invoking;//{ get; private set; }
    public bool SwitchDimension;// { get; private set; }
    public bool Use;//{ get; private set; }

    private bool initialized;

    private void Initialize()
    {
        // Get the Rewired Player object for this player.
        player = ReInput.players.GetPlayer(idName);
        initialized = true;
    }



    void Update()
    {
        if (!ReInput.isReady) return; 
        if (!initialized) Initialize(); 
        GetInput();
    }

    private void GetInput()
    {

        _direction.x = player.GetAxis("Move Horizontal");
        _direction.y = player.GetAxis("Move Vertical");
        Fire = player.GetButton("Fire");
        Use = player.GetButton("Use");
        Running = player.GetButton("Run");
         Jump = player.GetButton("Jump");
         AirJump = player.GetButton("AirJump");
         Invoking = player.GetButton("Invoke");
         SwitchDimension = player.GetButton("SwitchDimension");
         HasGround = groundChecker.IsColliding2D();
         HasWall = wallChecker.IsColliding2D();
    }


    void SetEnabledStateOnMapsInCategory(Player player, string categoryName, bool state)
    {
        player.controllers.maps.SetMapsEnabled(state, categoryName);
    }
}
