﻿using UnityEngine;
using System.Collections;

public class BlourakBossJungle : MonoBehaviour {


    public Juggler jugglerInfo;
    public float bulletsDepth = 3f;
    public float bodyDepth = 7f;
    public float handsDepth = 2.5f;
    public Vector3 [] bulletsPosition;
    public Vector3 handLeftPosition;
    public Vector3 handRightPosition;
    public Vector3 bodyPosition;

    public Transform [] tBullets;
    public Transform tHandLeft;
    public Transform defaultHandLeftPos;
    public Transform tHandRight;
    public Transform defaultHandRightPos;
    public Transform tBody;

    public float handSpeed = 42f;
    public float bodySpeed = 20f;
    public Vector3 bossPosAdjustement;

    private Vector3 bodyDirectionAVG;
    void Start() {

        bulletsPosition = new Vector3[jugglerInfo.balls.Length];
    }

    void Update()
    {

        Juggler ji = jugglerInfo;
        //Positionner
        for (int i = 0; i < ji.balls.Length; i++)
        {
            if (ji.balls[i].IsUsed)
            {
                Vector3 position = ji.GetBallSupposedPosition(i);
                position.z = bulletsDepth;
                bulletsPosition[i] = Camera.main.ViewportToWorldPoint(position);
                tBullets[i].position = bulletsPosition[i];
                tBullets[i].gameObject.SetActive(true);
            }
            else
            {
                tBullets[i].gameObject.SetActive(false);
            }
        }
        bool isOutLeft = false;
        Vector3 leftPos = ji.left._normalizedPosition;
        isOutLeft = !(leftPos.x > 0f && leftPos.x < 1f && leftPos.y > 0f && leftPos.y < 1f);
            leftPos.z = handsDepth;
            handLeftPosition = Camera.main.ViewportToWorldPoint(leftPos);
            tHandLeft.position = Vector3.MoveTowards(tHandLeft.position, isOutLeft ? defaultHandLeftPos.position : handLeftPosition, Time.deltaTime * handSpeed);

            bool isOutRight = false;
        Vector3 rightPos = ji.right._normalizedPosition;
            isOutRight = !(rightPos.x > 0f && rightPos.x < 1f && rightPos.y > 0f && rightPos.y < 1f);
            rightPos.z = handsDepth;
            handRightPosition = Camera.main.ViewportToWorldPoint(rightPos);
            tHandRight.position = Vector3.MoveTowards(tHandRight.position, isOutRight ? defaultHandRightPos.position : handRightPosition, Time.deltaTime * handSpeed);
        
        if ( !isOutLeft || !isOutRight)
        {

            Vector3 bodyPos = (leftPos + rightPos) / 2f;
            bodyPos.z = bodyDepth;
            bodyPosition = Camera.main.ViewportToWorldPoint(bodyPos);
            bodyPosition += bossPosAdjustement;

            bodyDirectionAVG = (20f * bodyDirectionAVG + 1f * bodyPosition) / 21f;

            tBody.position = Vector3.MoveTowards(tBody.position, bodyDirectionAVG, Time.deltaTime * bodySpeed);
        }
    }
}
