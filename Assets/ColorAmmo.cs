﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorAmmo : MonoBehaviour, IDimAmmoLoader {

    public int ammoAtStart = 3;
    public int maxAmmo = 3;
    public bool startWithRandomColor=true;
    public List<MultiDimBelong> ammo = new List<MultiDimBelong>();
    public List<MultiDimBelong> storedInfo = new List<MultiDimBelong>();



    void Start() {
        for (int i = 0; i < maxAmmo; i++)
        {
            storedInfo.Add( (MultiDimBelong) this.gameObject.AddComponent<MultiDimBelong>());
        }

        for (int i = 0; i < ammoAtStart; i++)
        {
            
            AddAmmo(new Dimensions.DimType [0]);
            if (startWithRandomColor) {
                ammo[i].AddRandomRGB();
            }
        }
    
    }


    public AmmoChangeDetected onChangeDetected;
    public void ListenToChange(AmmoChangeDetected listener)
    {
        onChangeDetected += listener;
    }
    void OnDestroy() {
        onChangeDetected = null;
    }

    public bool HasAmmo()
    {
        return ammo.Count>0;
    }

    public int GetAmmoCount()
    {
        return ammo.Count ;
    }

    public void UseAmmo()
    {
        ammo.RemoveAt(ammo.Count - 1);
        NotifyChange();
    }

    private void NotifyChange()
    {
        if (onChangeDetected != null)
            onChangeDetected(this);
    }
    public void AddAmmo( Dimensions.DimType [] colors)
    {
        if (ammo.Count >= maxAmmo) return;
        ammo.Add(storedInfo[ammo.Count]);
        SetColors(colors,ammo.Count-1);
        NotifyChange();
    }

    public Dimensions.DimType[] GetColors(int index)
    {
        if (index < 0 || index >= ammo.Count) return new Dimensions.DimType[0];
        else return ammo[index].GetBelongTo();
    }

    public void SetColors(Dimensions.DimType[] colors)
    {
        foreach (MultiDimBelong dimAmmo in ammo)
            dimAmmo.ResetDimWith(colors);
        NotifyChange();
    }

    public void SetColors(Dimensions.DimType[] colors, int index)
    {
        if (index < 0 || index >= ammo.Count) return;
        ammo[index].ResetDimWith(colors);
        NotifyChange();
    }

    public void AddColors(Dimensions.DimType[] colors)
    {
        foreach (MultiDimBelong dimAmmo in ammo)
            foreach (Dimensions.DimType col in colors)
                dimAmmo.AddTo(col);
        NotifyChange();
    }

    public void AddColors(Dimensions.DimType[] colors, int index)
    {
        if (index < 0 || index >= ammo.Count) return;
        foreach (Dimensions.DimType col in colors)
            ammo[index].AddTo(col);
        NotifyChange();
    }

    public void ResetColors()
    {
        foreach (MultiDimBelong dimAmmo in ammo)
            dimAmmo.ResetDim();
        NotifyChange();
    }

    public void ResetColors(int index)
    {
        if (index < 0 || index >= ammo.Count) return;
        ammo[index].ResetDim();
        NotifyChange();
    }



}
