﻿using UnityEngine;
using System.Collections;

public class BossBullet : MonoBehaviour {

    public string playerTagName = "Player";
    public MultiDimBelong dimension;
//    public Dimensions.DimType dommageType = Dimensions.DimType.White;
    public float minPctDommage=0.2f, maxPctDommage = 0.6f;
   


    public void OnEnable() {
        dimension.ResetDim();
    }

    public void OnTriggerEnter2D(Collider2D col) {

        if (col.gameObject.CompareTag(playerTagName)) {

            GameObject playerMainObject =col.attachedRigidbody.gameObject;
            //Debug.Log("Player collision", playerMainObject);
            PlayerColoredLife life = playerMainObject.GetComponent<PlayerColoredLife>() as PlayerColoredLife;
            if (life == null) life = playerMainObject.GetComponentInChildren<PlayerColoredLife>() as PlayerColoredLife;
            if (life != null)
            {

                dimension.ResetDim();
                dimension.AddRandomRGB();
                dimension.AddRandomRGB();
               // Debug.Log("Player taking dommage");
                Color color = Dimensions.GetColor(dimension.GetBelongTo());
                life.AddDarkness(color, Random.RandomRange(minPctDommage,maxPctDommage));
                if (life.GetDarknessPourcent() >= 1f)
                    dimension.ResetDim();
                

                
            }
        }
    }

}
