﻿using UnityEngine;
using System.Collections;

public class CreateTextureFromKinectDepth : MonoBehaviour {

    public KinectDepthAccessor accessor;
    public int divideBy=1;
    public bool inverseHorizontal;
    public Texture2D texture;

    public Texture2D GetTexture() { return texture; }
    public int MinDepth { get { return accessor.filterMin; } }
    public int MaxDepth { get { return accessor.filterMax; } }

    public Color[] colors;
    private bool isInit;
    private Color[] textureColors;
    public float refreshTime=0.2f;
    void Start() {

        InvokeRepeating("ResfreshTexture", 0, refreshTime);
    }

    void ResfreshTexture()
    {
        if (accessor == null  ) return;
        int width = accessor.DepthWidth / divideBy, height = accessor.DepthHeight / divideBy;
        if (height <= 0 || width <= 0) return;
 
        if (!isInit)
        {
            texture = new Texture2D(width, height, TextureFormat.ARGB32, false);
            texture.name = "KinectDepth";
            textureColors = new Color[width*height];
            isInit = true;
        }
        float maxSousMin = (float) MaxDepth - MinDepth;
        
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width ; x++)
            {

                ushort depthValue = accessor.GetDepthValueAt( x * divideBy,y * divideBy);
                if (depthValue == 0)
                    textureColors[(inverseHorizontal ? x : width - 1 - x) + y * width] = Color.black;
                else { 
                    float pct = (float)(depthValue - MinDepth) /maxSousMin;
                    Color color = ColorsGetter.GetLinkedColor(pct, colors);
                    textureColors[(inverseHorizontal ? x : width - 1 - x) + y * width] = color;
                }         
            }
        }
        texture.SetPixels(textureColors);
        texture.Apply();
    }
}
