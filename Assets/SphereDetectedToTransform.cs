﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SphereDetectedToTransform : MonoBehaviour {

    public SphereDetector_KinectDepth sphereDetected;

    public Transform[] objects;
    public float zDistance = 6f;
	
	
	void Update () {

        List<PixelZone> spheres = sphereDetected.spheres;
        for (int i = 0; i < objects.Length; i++)
        {

            if (i < spheres.Count)
            {
                Vector3 pos = spheres[i].CenterNormalized;
                pos.z = zDistance;
                objects[i].position = Camera.main.ViewportToWorldPoint(pos);
                objects[i].gameObject.SetActive(true);
            }
            else
            {
                objects[i].position = Vector3.zero;
                objects[i].gameObject.SetActive(false);
            }
        }
	
	}
}
