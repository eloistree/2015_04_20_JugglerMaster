﻿using UnityEngine;
using System.Collections;

public class WebCamTest : MonoBehaviour {

    public WebCamTexture[] webcame;
    public GameObject [] applyTexture;
    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        webcame = new WebCamTexture[devices.Length];
        int i = 0;
        while (i < devices.Length)
        {
            WebCamTexture wTexture = new WebCamTexture(devices[i].name);
            webcame[i] = wTexture;
            Debug.Log(devices[i].name);
            i++;
            wTexture.Play();
        }
    }

    void Update() {
        int i = 0;
        while (i < webcame.Length)
        {
            applyTexture[i].GetComponent<Renderer>().material.SetTexture(0, webcame[i]);
            
            i++;

        }
    
    }
    void OnDestroy() {

        int i = 0;
        while (i < webcame.Length)
        {
            webcame[i].Stop(); i++;
            
        }
    }
}
