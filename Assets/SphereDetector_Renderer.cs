﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SphereDetector_Renderer : MonoBehaviour {

    public MeshRenderer renderer;
    public float pctToBeSphere=0.20f;
    public float minSize = 20f;
    public List<PixelZone> spheres;

    void Update()
    {

        //UshortExemple();
        //TextureExemple();
        spheres.Clear();
        List<PixelZone> zones = new List<PixelZone>();
        RealTextureExemple((Texture2D) renderer.material.mainTexture, ref zones);
        foreach (PixelZone pz in zones)
        {
            float nearest = pz.CenterToNearestBorder;
            float farest = pz.CenterToFarestBorder;
            
            float range = farest * pctToBeSphere;

            if ((nearest>farest-range && nearest<farest+range) && farest>minSize  )

                spheres.Add(pz);
 
        }

    }
    private static void RealTextureExemple(Texture2D texture, ref List<PixelZone> zones)
    {
        if (texture == null) return;
        zones = PixelZone.GetZone<Color>(Color.black, (x, y) => { return texture.GetPixel(x, y); }, texture.width, texture.height);
    }
	
}
