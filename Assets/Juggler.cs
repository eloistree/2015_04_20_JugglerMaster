﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Juggler : MonoBehaviour {
    public SphereDetector_KinectDepth sphereDetection;
    public KinectJugglerBasicData jugglerBasicData;

    public JBall[] balls = new JBall[] { new JBall() , new JBall(), new JBall(), new JBall(), new JBall() };
    public JHand left = new JHand();
    public JHand right = new JHand();

    
    public float trackingLostTime = 0.3f;
    public float simGravity = 9.81f;

    public float handRangePct=0.05f;
    //public SphereDetector spheres;
	// Use this for initialization
	void Start () {

        foreach (JBall b in balls)
            b._lostTrackingDelay = trackingLostTime;
	}
	
	// Update is called once per frame
    List<JBall> used = new List<JBall>();
	void Update () {

        left.SetNewPosition(jugglerBasicData.handLeft.DepthCoordinateNormalized);
        right.SetNewPosition(jugglerBasicData.handRight.DepthCoordinateNormalized);

        foreach (JBall b in balls)
            b.UpdateTimeInfo(Time.deltaTime);

        List<PixelZone> spheresInAir = sphereDetection.spheres;
        used.Clear();
        foreach (PixelZone pz in spheresInAir) {
            if(! IsInHandRegion(pz.CenterNormalized)){
            JBall nearBall = GetNearestActiveBallPossibility(pz.CenterNormalized, used);
            if (nearBall == null){
                nearBall = GetFirstBallAvailable(used);
                JHand hand = GetNearestHand(pz.CenterNormalized);
                nearBall.SetNewPosition(hand._normalizedPosition,0f);
                nearBall.SetNewPosition(pz.CenterNormalized,Time.deltaTime);
            } else {

                nearBall.SetNewPosition(pz.CenterNormalized,Time.deltaTime);
            }
            used.Add(nearBall);
            if (nearBall == null) Debug.LogWarning("Should not be null");

            nearBall.SetNewPosition(pz.CenterNormalized);
            }
        }

	}

    private bool IsInHandRegion(Vector2 normPos)
    {
        return InHandRegion(normPos)!=null;
    }
    private JHand InHandRegion(Vector2 normPos)
    {

        if (Vector2.Distance(left._normalizedPosition, normPos) < handRangePct) return left;
        if (Vector2.Distance(right._normalizedPosition, normPos) < handRangePct) return right;
        return null;
    }

    private JHand GetNearestHand(Vector2 normPos)
    {
        float dist = Vector2.Distance( left._normalizedPosition,normPos);
        return dist < Vector2.Distance(right._normalizedPosition, normPos) ? left:right;
    }

    private JBall GetFirstBallAvailable( List<JBall>  alreadyAssigned)
    {
        foreach (JBall b in balls)
        {
            if (!b.IsUsed && !alreadyAssigned.Contains(b)) return b;
        }
        return null;
    }

    private JBall GetNearestActiveBallPossibility(Vector2 normPos, List<JBall>  alreadyAssigned)
    {
        JBall result=null;
        float lessDistance=float.MaxValue;
        foreach (JBall b in balls)
        {
            if (b.IsUsed && !alreadyAssigned.Contains(b)) {
                float dist = Vector2.Distance(normPos, b._normalizedPosition);
                if (result == null || ( result != null && dist < lessDistance ) )
                {
                    lessDistance = dist;
                    result = b;
                }
            }
        }
        return result;
            
    }

    public abstract class JuggleElement {

        public static Pool<PointInTime> PointsPool = new Pool<PointInTime>(1000);
        public Vector2 _normalizedPosition;
        public Vector2 _normalizedLastPosition;
        public Vector2 _normalizedDirection;
        public float _timeSinceNoContact;
        public float _lostTrackingDelay =1f;
        public float _timeCount;
        public bool IsUsed;


        public List<PointInTime> _path = new List<PointInTime>();
        public delegate void OnPathStart(JuggleElement element, Vector2 normPosition);
        public delegate void OnPathNewPoint(JuggleElement element, Vector2 normPosition, float when);
        public delegate void OnPathEnd(JuggleElement element, Vector2[] pathOfNormPosition, float duration);
        public OnPathStart onPathStart;
        public OnPathNewPoint onPathNewPoint;
        public OnPathEnd onPathEnd;

       public void SetNewPosition(Vector2 newPositionNormalized, float time = 0f) {
            _normalizedLastPosition = _normalizedPosition;
            _normalizedPosition = newPositionNormalized;
            _normalizedDirection = (_normalizedPosition - _normalizedLastPosition)*_timeSinceNoContact;

            RecordPathPoint(ref newPositionNormalized);
  
           ResetNoInfoTimer();
            if (time != 0f) UpdateTimeInfo(time);
        }

       private void RecordPathPoint(ref Vector2 point)
       {
           if (_path.Count > 0 && _path[_path.Count - 1].point == point) return;

           PointInTime nextPoint = JuggleElement.PointsPool.GetNext();
           nextPoint.Reset();
           nextPoint.point = point;
           nextPoint.since = _timeCount;
           _path.Add(nextPoint);

           if ( _path.Count == 1)
           {
               if (onPathStart != null) {
                   onPathStart(this, point);
               }

           }
           else {
               if (onPathNewPoint != null)
               {
                   onPathNewPoint(this, point, _timeCount);
               }
           }
       }
     
       public void UpdateTimeInfo(float deltaTime) {
           _timeSinceNoContact += deltaTime;
           _timeCount += deltaTime;
           if (IsUsed && _timeSinceNoContact > _lostTrackingDelay) {
               ResetInfo();
               IsUsed = false;
           }
       }
       public void ResetNoInfoTimer() { _timeSinceNoContact = 0; IsUsed = true; }
       public void ResetInfo()
       {

           if (onPathEnd != null)
           {
                Vector2 [] pathTab = new Vector2[_path.Count];
                for (int i = 0; i < pathTab.Length; i++)
			    {
	    		     pathTab[i] =_path[i].point;
			    }    
                onPathEnd(this,pathTab, _timeCount);
           }
           _normalizedPosition = Vector2.zero;
           _normalizedLastPosition = Vector2.zero;
           _normalizedDirection = Vector2.zero;
           _timeCount = 0f;
           _path.Clear();
       }
    }
    [System.Serializable]
    public class JBall : JuggleElement
    {
        //public Vector2 VulgarPredictionPosition(float time) {
        //    return _normalizedPosition + _normalizedDirection * _normalizedSpeed * time;
        //}

        //public enum BallState { NotUsed, InAir, InHand, ContactLost }
        //public BallState _ballState = BallState.NotUsed;
        //public JHand _shouldBeInHand;

        internal Vector3 GetShoudBePosition()
        {
            return GetShoudBePosition(_timeSinceNoContact,0f);
        }

        internal Vector3 GetShoudBePosition(float time, float gravity =9.81f)
        {

            Vector2 result = _normalizedPosition;
            result += _normalizedDirection * time;
            result.y -= gravity * time*time;

            return result;
        }

        public bool IsInGoodDirection(Vector2 nextPosition, float maxAngleDirectionAllow)
        {
            if (_path.Count <= 1) return true;
            int lastIndex = _path.Count-1;
            Vector2 currentDirection = _path[lastIndex].point - _path[lastIndex - 1].point;
            Vector2 nextDirection = nextPosition - _path[lastIndex].point;
            float angle = Vector2.Angle(currentDirection, nextDirection);
            Debug.Log("Angle: 80 vs "+angle +  " current "+ currentDirection+ "   next "+nextDirection);
            return maxAngleDirectionAllow > angle;

        }
    }
    [System.Serializable]
    public class JHand : JuggleElement
    {

    }

    public Vector3 GetBallSupposedPosition(int i)
    {
        return balls[i].GetShoudBePosition(balls[i]._timeSinceNoContact, simGravity); 
    }
    [System.Serializable]
    public struct PointInTime {
        public Vector2 point;
        public float since;

        public void Reset() {
            point = Vector2.zero;
            since = 0;
        }
    }

   
}
