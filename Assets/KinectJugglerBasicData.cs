﻿using UnityEngine;
using System.Collections;

public class KinectJugglerBasicData : MonoBehaviour {

    public KinectUserSelection userSelection;
    public KinectInfo head;
    public KinectInfo midSpine;
    public KinectInfo handLeft;
    public KinectInfo handRight;
	
	
    void Update()
    {
        long user = userSelection.GetUserId();
        RefreshInfo(ref head, user, (int)KinectInterop.JointType.Head);

        RefreshInfo(ref midSpine, user, (int)KinectInterop.JointType.SpineMid);

        RefreshInfo(ref handLeft, user, (int)KinectInterop.JointType.HandLeft);

        RefreshInfo(ref handRight, user, (int)KinectInterop.JointType.HandRight);

    }

    private void RefreshInfo(ref KinectInfo jointInfo, long userId, int jointId)
    {
        KinectManager manager = KinectManager.Instance;
        int width = manager.GetDepthImageWidth(), height = manager.GetDepthImageHeight();
        jointInfo.Height = height;
        jointInfo.Width = width;
        if (manager.IsUserDetected())
        {
            if (manager.IsJointTracked(userId, jointId))
            {
                Vector3 posJoint = manager.GetJointKinectPosition(userId, jointId);

                if (posJoint != Vector3.zero)
                {
                    // 3d position to depth
                    Vector2 posDepth = manager.MapSpacePointToDepthCoords(posJoint);
                    ushort depth  = manager.GetDepthForPixel((int)posDepth.x, (int)posDepth.y);
                    if (posDepth != Vector2.zero)
                        jointInfo.DepthCoordinate = posDepth;
                    if (depth != 0)
                        jointInfo.Depth = depth;
                }
            }

        }
    }

    [System.Serializable]
    public struct KinectInfo
    {
        public Vector2 DepthCoordinate;
        public int Depth;
        public int Width;
        public int Height;
        public Vector2 DepthCoordinateNormalized { get {
            Vector2 result = DepthCoordinate;
            result.x /= Width;
            result.y /= Height;
            result.y = 1f - result.y;
            return result;
        } }
    }
}
