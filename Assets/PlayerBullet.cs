﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

    public string playerTagName = "Player";
    public string bossTagName = "Boss";
    public string stationTagName = "ColorStation";
    public MultiDimBelong dimensionBelong;
    public float pourcentDommage = 0.1f;

    public void Awake() {
        if (dimensionBelong == null) Destroy(this);
    }

    public void OnCollisionEnter2D(Collision2D col)
    {

        Debug.Log(col.gameObject.tag);
        if (col.gameObject.CompareTag(playerTagName))
        {
            IDimAmmoLoader ammo = col.gameObject.GetComponentInChildren(typeof(IDimAmmoLoader)) as IDimAmmoLoader;
            if (ammo != null)
            {
                ammo.AddAmmo(dimensionBelong.GetBelongTo());
                this.gameObject.SetActive(false);
            }
        }
        if (col.gameObject.CompareTag(bossTagName))
        {
            ColoredLife bossLife = col.gameObject.GetComponentInChildren<ColoredLife>() as ColoredLife;
            if (bossLife != null)
            {
                Color color = Dimensions.GetColor(dimensionBelong.GetDimensionColor());
                bossLife.AddClarity(color, pourcentDommage);
                dimensionBelong.ResetDim();
            }
        }
        

    }

    public void OnTriggerEnter2D(Collider2D col)
    {

        Debug.Log(col.gameObject.tag);
        if (col.gameObject.CompareTag(stationTagName))
        {
            DimensionStation station = col.gameObject.GetComponent<DimensionStation>() as DimensionStation;
            if (station != null)
            {
                dimensionBelong.AddTo(station.dimension);
                
            }
        }

    }
}
