﻿using UnityEngine;
using System.Collections;

public class SpriteColorEqualMultiDim : MonoBehaviour {

    public MultiDimBelong dimensionObserved;
    public SpriteRenderer spriteRender;
    public Color currentColor;
    public Dimensions.DimColor lastColor;

    public bool autoComplete=true;
  
    void Update() {
     //   spriteRender.color = Dimensions.GetColor(dimensionObserved.GetDimensionColor());
    
    }

    private void RefreshColor(MultiDimBelong who, Dimensions.DimType newDim, Dimensions.DimColor newColor)
    {
        Color col = Dimensions.GetColor(newColor);

        //Debug.Log("Bouuuuuhh Color: " + col,this.gameObject);
        spriteRender.color = currentColor = col;
    }


    void Awake()
    {
        if (autoComplete)
        {
            if (dimensionObserved == null)
                dimensionObserved = GetComponent<MultiDimBelong>() as MultiDimBelong;
            if (spriteRender == null)
                spriteRender = GetComponent<SpriteRenderer>() as SpriteRenderer;

        }
        dimensionObserved.onBelongToNewDimension += RefreshColor;
    }

    void OnDestroy()
    {
        dimensionObserved.onBelongToNewDimension -= RefreshColor;
    }

}
