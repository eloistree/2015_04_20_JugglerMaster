﻿using UnityEngine;
using System.Collections;

public class FilterFromJuggleToDepth : MonoBehaviour {

    public KinectDepthAccessor accessor;
    public KinectJugglerBasicData jugglerBasic;
    [Range(0,6000)]
    public int range = 300;
    [Range(0, 6000)]
    public int start = 50;

	void Update () {
        ushort playerDepth =(ushort) jugglerBasic.head.Depth;
        accessor.filterMin = playerDepth-start - range;
        accessor.filterMax = playerDepth-start ;
	
	}
}
