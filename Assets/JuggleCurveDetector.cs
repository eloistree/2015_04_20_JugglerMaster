﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JuggleCurveDetector : MonoBehaviour {

	
    
    public Juggler juggler;
    public float angleBifurcationAllowed = 80f;
    public int minPointNumber = 2;
    public Dictionary<Juggler.JuggleElement, CurveBuilder> curveBuilder = new Dictionary<Juggler.JuggleElement, CurveBuilder>();
    // Use this for initialization

    void Start()
    {

        foreach (Juggler.JBall ball in juggler.balls)
        {
            //           ball.onPathEnd += DrawPath;
            ball.onPathNewPoint += AddPathPoint;
            ball.onPathEnd += FlushCurveBuilder;
        }
    }

    private void FlushCurveBuilder(Juggler.JuggleElement element, Vector2[] pathOfNormPosition, float duration)
    {

        if (!curveBuilder.ContainsKey(element))
        {
            curveBuilder.Add(element, new CurveBuilder(angleBifurcationAllowed, minPointNumber));
        }
        CurveBuilder builder = curveBuilder[element];
        Vector2[] curveDetected = builder.Flush();
        if (curveDetected != null)
            DrawLine(curveDetected, displayTime);
    }

    void OnDestroy()
    {
        foreach (Juggler.JBall ball in juggler.balls)
        {
            //          ball.onPathEnd -= DrawPath;
            ball.onPathNewPoint -= AddPathPoint;
            ball.onPathEnd -= FlushCurveBuilder;
        }
    }



    private void DrawLine(Vector2[] curveDetected, float time)
    {
        DrawLine(curveDetected, new Color(Random.RandomRange(0f, 1f), Random.RandomRange(0f, 1f), Random.RandomRange(0f, 1f)), time);
    }

    private void DrawLine( Vector2[] line, Color color,float time=10f)
    {
        if (line.Length > 1)
            for (int i = 1; i < line.Length; i++)
            {
                Debug.DrawLine(GetRealPoint(line[i - 1]), GetRealPoint(line[i]), color, time);


            }
    }

    private void AddPathPoint(Juggler.JuggleElement element, Vector2 normPosition, float when)
    {
        if (!curveBuilder.ContainsKey(element))
        {
            curveBuilder.Add(element, new CurveBuilder(angleBifurcationAllowed, minPointNumber));
        }
        CurveBuilder builder = curveBuilder[element];
        Vector2[] curveDetected = builder.GetCurveIfDetectedWithNewElement(normPosition);
        if (curveDetected != null)
            DrawLine(curveDetected,displayTime);
    }

    public float displayTime = 60f;

  

    public Vector3 GetRealPoint(Vector2 normPoint)
    {
        return Camera.main.ViewportToWorldPoint(normPoint);

    }


    public class CurveBuilder {

        List<Vector2> points = new List<Vector2>();
        public bool curveIncrease = true;
        private float angleBifurcationAllowed;
        private int minPointNumber;

        public CurveBuilder(float angleBifurcationAllowed, int minPointNumber)
        {
            // TODO: Complete member initialization
            this.angleBifurcationAllowed = angleBifurcationAllowed;
            this.minPointNumber = minPointNumber;
        }

        public Vector2[] GetCurveIfDetectedWithNewElement(Vector2 newPoint)
        {
            Vector2[] result = null;
            if (points.Count == 0)
            {
                points.Add(newPoint);
                curveIncrease = true;
                return null;
            }
            Vector2 direction = newPoint - points[points.Count-1];
            bool isCurrentIncreasing = direction.y>=0f;
            //si la courbe remonter après avoir descendu
            //je créer une nouvelle courbe
            if ((!curveIncrease && isCurrentIncreasing) || (!curveIncrease && !IsInGoodDirection(newPoint)))
            {
                result = new Vector2[points.Count];
                points.CopyTo(result, 0);
                points.Clear();
                curveIncrease = true;
                points.Add(newPoint);
            }
            else {
                points.Add(newPoint);
                curveIncrease = isCurrentIncreasing;
            }

            if (result != null && result.Length <= minPointNumber)
                result = null;
            return result;
        }


        private bool IsInGoodDirection(Vector2 nextPosition)
        {
            if (points.Count <= 1) return true;
            int lastIndex = points.Count-1;
            Vector2 currentDirection = points[lastIndex] - points[lastIndex - 1];
            Vector2 nextDirection = nextPosition - points[lastIndex];
            float angle = Vector2.Angle(currentDirection, nextDirection);
            //Debug.Log("Angle: 80 vs "+angle +  " current "+ currentDirection+ "   next "+nextDirection);
            return  angle < angleBifurcationAllowed ;

        }

        public Vector2[] Flush()
        {
            Vector2[] result = new Vector2[points.Count];
            points.CopyTo(result, 0);
            points.Clear();

            if (result != null && result.Length <= minPointNumber)
                result = null;
            return result;
        }
    }
}

