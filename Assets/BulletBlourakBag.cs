﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletBlourakBag : MonoBehaviour {


    public IDimAmmoLoader ammoLoader;
    public MonoBehaviour sc_ammoLoader;
    public List<MultiDimBelong> visibleElement = new List<MultiDimBelong>();

    void Start()
    {
        if (sc_ammoLoader != null)
        {
            ammoLoader = (IDimAmmoLoader)sc_ammoLoader;
            ammoLoader.ListenToChange(Refresh);
        }
        Refresh();
    }

    private void Refresh(IAmmoLoader obj)
    {
        Refresh();        
    }
    
   
    public void  Refresh()
    {
        for (int i = 0; i < visibleElement.Count; i++)
        {
            if (i < ammoLoader.GetAmmoCount())
            {
                visibleElement[i].ResetDimWith(ammoLoader.GetColors(i));
                visibleElement[i].gameObject.SetActive(true);
            }
            else { 
             visibleElement[i].ResetDim();
             visibleElement[i].gameObject.SetActive(false);
            }
        }
    }
}
