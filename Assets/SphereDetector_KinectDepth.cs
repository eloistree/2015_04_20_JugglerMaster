﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SphereDetector_KinectDepth : MonoBehaviour {

    public KinectDepthAccessor depthAccessor;
    public float pctToBeSphere = 0.20f;
    public float minSizePct = 0.05f;
    public float maxSphereSizePct = 0.07f;
    public float maxOtherSizePct = 0.1f;
    public List<PixelZone> spheres;
    public List<PixelZone> others;

    void Update()
    {
        if (depthAccessor == null) return;
        //UshortExemple();
        //TextureExemple();
        spheres.Clear();
        others.Clear();
        List<PixelZone> zones = PixelZone.GetZone<ushort>(0, GetDepth, depthAccessor.DepthWidth, depthAccessor.DepthHeight);

        foreach (PixelZone pz in zones)
        {
            float nearest = pz.CenterToNearestBorderNormalized;
            float farest = pz.CenterToFarestBorderNormalized;
            
            float range = farest * pctToBeSphere;

            if (farest > minSizePct ) {
                if ((nearest > farest - range && nearest < farest + range) && farest < maxSphereSizePct)
                    spheres.Add(pz);
                else if (farest < maxOtherSizePct)  others.Add(pz);
            }
 
        }

    }

    private ushort GetDepth(int x, int y)
    {
        return  depthAccessor.GetDepthValueAt( x,y); 
    }
    //private static void RealTextureExemple(ref ushort [,] depth,int width, int height, ref List<PixelZone> zones)
    //{
    //    zones = PixelZone.GetZone<ushort>(0, (x, y) => { return depth[y*2, x*2]; }, width / 2, height / 2);
    //}
	
}
