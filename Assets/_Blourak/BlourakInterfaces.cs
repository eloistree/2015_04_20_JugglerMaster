﻿using UnityEngine;
using System.Collections;


public interface IWeapon
{
    bool Fire();
    bool Fire(Vector3 direction);
    bool HasAmmo();
    int GetAmmoCount();
    IAmmoLoader GetAmmoLoader();
}

public delegate void AmmoChangeDetected(IAmmoLoader obj);
public interface IAmmoLoader
{
    void ListenToChange(AmmoChangeDetected listener);
    bool HasAmmo();
    int GetAmmoCount();
    void UseAmmo();
}

public interface ICharacterBasicInputAccess
{
    Vector3 GetDirection();
    bool IsUseActive();
    bool IsFireActive();
    bool IsJumpActive();
}
public interface ICharacterAdvancedAccess : ICharacterBasicInputAccess
{
    bool IsInvokingPlayer();
    bool IsInvokingOtherDimension();
    bool IsAirJumpActive();
}

public interface IDimensionHolder
{

    Dimensions.DimType[] GetColors( int index );

    void SetColors(Dimensions.DimType[] colors);
    void SetColors(Dimensions.DimType[] colors, int index);

    void AddColors(Dimensions.DimType[] colors);
    void AddColors(Dimensions.DimType[] colors, int index);

    void ResetColors();
    void ResetColors(int index);

}

public interface IDimAmmoLoader : IAmmoLoader, IDimensionHolder
{
    void AddAmmo(Dimensions.DimType[] colors);
}