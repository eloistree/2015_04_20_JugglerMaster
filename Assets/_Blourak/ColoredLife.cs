﻿using UnityEngine;
using System.Collections;

public class ColoredLife : MonoBehaviour
{

    public Color life;
    public Color greenBlueUpLife;
    public Color greenRedLeftLife;
    public Color redBlueRightLife;
    private Color lastLife;

    public void AddClarity(Color value, float pourcent)
    {
        if (pourcent < 0f) pourcent = 0f;
        life += value * pourcent;
        Clamp(ref life, 0f, 1f);
    }

    public void AddDarkness(Color value, float pourcent)
    {
        if (pourcent < 0f) pourcent = 0f;
        life -= value * pourcent;
        Clamp(ref life, 0f, 1f);
    }

    public bool IsDeath()
    {
        return life.r >= 1f && life.g >= 1f && life.b >= 1f;
    }
    public float GetClarityPourcent()
    {
        float pct = 0f;
        pct +=  life.r * 0.334f;
        pct +=  life.g * 0.334f;
        pct +=  life.b * 0.334f;
        return Mathf.Clamp(pct, 0f, 1f);
    }
    public float GetDarknessPourcent() {
        return 1f - GetClarityPourcent();
    }

    private void Clamp(ref Color life, float min, float max)
    {
        life.a = 1f;
        life.r = Mathf.Clamp(life.r, 0f, 1f);
        life.g = Mathf.Clamp(life.g, 0f, 1f);
        life.b = Mathf.Clamp(life.b, 0f, 1f);
    }

    void Awake()
    {
        life.a = 1f;
        greenBlueUpLife = greenRedLeftLife = redBlueRightLife =life;


        RefreshLifeColor(life);
    }

    void Update()
    {
        if (life != lastLife)
        {
            lastLife = life;
            RefreshLifeColor(life);
        }

    }

    protected virtual void RefreshLifeColor(Color life)
    {
        greenBlueUpLife.g = life.g;
        greenBlueUpLife.b = life.b;

        greenRedLeftLife.g = life.g;
        greenRedLeftLife.r = life.r;

        redBlueRightLife.r = life.r;
        redBlueRightLife.b = life.b;
    }
}