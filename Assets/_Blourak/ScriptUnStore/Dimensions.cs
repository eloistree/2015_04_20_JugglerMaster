﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Dimensions : MonoBehaviour {

    private static Dimensions _instance;

    public static Dimensions Instance
    {
        get { return _instance; }
       private set { 
           if(_instance==null)
               _instance= value;
       }
    }



    public DimType currentDimension;
    public enum DimType { Red, Green, Blue,Black, White }
    public enum DimColor { Black,Red, Green, Blue,SkyBlue,Purple,Yellow, White }

    public GameObject dimRed;
    public GameObject dimGreen;
    public GameObject dimBlue;
    public GameObject dimWhite;
    public GameObject dimBlack;
    
    public Color black;
    public Color red;
    public Color green;
    public Color blue;
    public Color skyBlue;
    public Color purple;
    public Color yellow;
    public Color white;



    public delegate void OnDimensionSwitch(GameObject dimensionRoot, Color mainColor, Dimensions.DimType from, Dimensions.DimType to);
    public OnDimensionSwitch OnSwitch;
    public delegate void BeforeDimensionSwitch(GameObject dimensionRoot, Color mainColor, Dimensions.DimType from, Dimensions.DimType to);
    public BeforeDimensionSwitch BeforeSwitch;
    public delegate void AfterDimensionSwitch(GameObject dimensionRoot, Color mainColor, Dimensions.DimType from, Dimensions.DimType to);
    public AfterDimensionSwitch AfterSwitch;

    public void Awake() 
    {
        Instance = this;
        SwitchDimension(currentDimension);

    }

    public void SwitchDimension(DimType dimension)
    {
        if (!CheckValidityParams()) return;
        DimType old = currentDimension;
        currentDimension = dimension;


        UnityEngine.Color colorNewDim = Color.white;
        GameObject toActivate = null;
        switch (dimension) 
        {
            case DimType.Blue:
                toActivate = dimBlue;
                colorNewDim = blue;
                break;
            case DimType.Red:
                toActivate = dimRed;
                colorNewDim = red;
                break;
            case DimType.Green:
                toActivate = dimGreen;
                colorNewDim = green;
                break;
            case DimType.Black:
                toActivate = dimBlack;
                colorNewDim = black;
                break;
            case DimType.White:
                toActivate = dimWhite;
                colorNewDim = white;
                break;
        }

        Camera.main.backgroundColor = colorNewDim;

        if (BeforeSwitch != null)
            BeforeSwitch(toActivate, colorNewDim,old,currentDimension);
        
        dimWhite.SetActive(false);
        dimBlack.SetActive(false);
        dimRed.SetActive(false);
        dimGreen.SetActive(false);
        dimBlue.SetActive(false);

        toActivate.SetActive(true);
        if(OnSwitch!=null)
            OnSwitch(toActivate, colorNewDim, old, currentDimension);

        if (AfterSwitch != null)
            AfterSwitch(toActivate, colorNewDim, old, currentDimension);
    }

    private bool CheckValidityParams()
    {
        return dimBlue != null && dimGreen != null && dimRed != null && dimWhite != null;
    }

    public static DimColor GetDimensionColor(List<DimType> belongDim,bool withBlackDominance=false, bool withWhiteDominance=false)
    {
        if(withBlackDominance && HasIn(DimType.Black, ref belongDim)) return DimColor.Black;
        if(withWhiteDominance && HasIn(DimType.White, ref belongDim)) return DimColor.White;
        bool hasRed = HasIn(DimType.Red, ref belongDim);
        bool hasGreen = HasIn(DimType.Green, ref belongDim);
        bool hasBlue = HasIn(DimType.Blue, ref belongDim);

        if (!hasRed && !hasGreen && !hasBlue) return DimColor.Black;

        if (hasRed && !hasGreen && !hasBlue) return DimColor.Red;
        if (!hasRed && hasGreen && !hasBlue) return DimColor.Green;
        if (!hasRed && !hasGreen && hasBlue) return DimColor.Blue;

        if (!hasRed && hasGreen && hasBlue) return DimColor.SkyBlue;
        if (hasRed && !hasGreen && hasBlue) return DimColor.Purple;
        if (hasRed && hasGreen && !hasBlue) return DimColor.Yellow;
        
        if (hasRed && hasGreen && hasBlue) return DimColor.White;

        return DimColor.Black;
    }

    private static bool HasIn(DimType dim ,ref List<DimType> belongDim)
    {
       return belongDim.Contains(dim);
    }

    public static Color GetColor(DimType[] dimType)
    {
        List<DimType> dimTypeList = new List<DimType>(dimType);
        return GetColor(dimTypeList);

    }
    
    public static Color GetColor(List<DimType> dimTypeList)
    {
        DimColor dColor = GetDimensionColor(dimTypeList);
        return GetColor(dColor);
    }

    internal static Color GetColor(DimColor dimColor)
    {
        Color color = Color.white;
        
        switch (dimColor)
        {
            case DimColor.Black: color = Instance.black; break;
            case DimColor.Red: color = Instance.red; break;
            case DimColor.Green: color = Instance.green; break;
            case DimColor.Blue: color = Instance.blue; break;
            case DimColor.Yellow: color = Instance.yellow; break;
            case DimColor.Purple: color = Instance.purple; break;
            case DimColor.SkyBlue: color = Instance.skyBlue; break;
            case DimColor.White: color = Instance.white; break;

        }

        return color;
    }

    public  static Color GetColor(DimType lastDimension)
    {
        DimColor color = GetDimensionColor(lastDimension);
        return GetColor(color);

    }

    private static DimColor GetDimensionColor(DimType lastDimension)
    {
        switch (lastDimension)
        {

            case DimType.Black: return DimColor.Black;
            case DimType.Red: return DimColor.Red;
            case DimType.Green: return DimColor.Green;
            case DimType.Blue: return DimColor.Blue;
            case DimType.White: return DimColor.White;

        }
        return DimColor.Black;
    }
}
