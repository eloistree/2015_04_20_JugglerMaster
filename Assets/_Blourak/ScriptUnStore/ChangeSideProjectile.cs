﻿using UnityEngine;
using System.Collections;

public class ChangeSideProjectile : MonoBehaviour {

    public MultiDimBelong dim;
    public string newLayerWhenGood = "WhiteProjectile";
    public ProjectileMove moveControl;
    public string bossTagName = "Boss";
    public bool hasChangeSide;
    void Start() 
    {
        if (dim) 
        {
            dim.onBelongToNewDimension +=(who, d, c)=>
            {
                if (dim && BelongToRGB(dim)) 
                {
                    if(newLayerWhenGood!=null && newLayerWhenGood.Length>0)
                    gameObject.layer = LayerMask.NameToLayer(newLayerWhenGood);
                    if (moveControl)
                    {
                        GameObject gamo = GameObject.FindGameObjectWithTag(bossTagName) as GameObject;
                        if (gamo)
                            moveControl.target = gamo.transform;
                        else moveControl.target = null;

                        print("Has change of side: " + gamo);

                    }
                    hasChangeSide = true;
                }
            };        
        }

        if (Dimensions.Instance) 
        Dimensions.Instance.AfterSwitch+=OnSwitchSide;
       
        
    }
    public void OnDestroy() {

        if (Dimensions.Instance)
            Dimensions.Instance.AfterSwitch -= OnSwitchSide;
    }

    public void OnSwitchSide(GameObject dimensionRoot, Color mainColor, Dimensions.DimType from, Dimensions.DimType to) 
    {  
            if (!hasChangeSide) return;
            gameObject.layer = LayerMask.NameToLayer(newLayerWhenGood);
            if (moveControl)
            {
                GameObject gamo = GameObject.FindGameObjectWithTag(bossTagName) as GameObject;
                if (gamo)
                    moveControl.target = gamo.transform;
                else moveControl.target = null;

                print("Has change of side: " + gamo);

            }

    
    }

    private bool BelongToRGB(MultiDimBelong dim)
    {
        if (!dim) return false;
        return dim.BelongTo(Dimensions.DimType.Red)
        && dim.BelongTo(Dimensions.DimType.Green)
        && dim.BelongTo(Dimensions.DimType.Blue);
    }

	
}
