﻿using UnityEngine;
using System.Collections;
using be.primd.blourak;
public class PlayerCameraFocus : CameraFocusElement {

    public be.primd.blourak.TrackedPoint playerTrackedPoint;
    public float priotityFocus = 1f;
    public float size = 2f;
    public Vector3 adjustment;

	void Start () {
       
        if(playerTrackedPoint!=null)
            CameraFocus.AddFocus(this);
        
	}
	
	

    public override Vector3 GetCameraPosition()
    {
        Vector3 pos = playerTrackedPoint.Position;
        Vector3 dir = playerTrackedPoint.Direction;
        float speed = playerTrackedPoint.Speed;
        
        pos.y += adjustment.y;
        if (Mathf.Abs(dir.x) > 0.5f) 
            pos.x += dir.x < 0 ? -adjustment.x : adjustment.x;
        return pos  ;
    }

    public override float GetCameraSize()
    {
        return size;
    }

    public override float GetPriority()
    {
        return priotityFocus;
    }
}
