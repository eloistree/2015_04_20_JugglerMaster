﻿using UnityEngine;
using System.Collections;
using System;

 [RequireComponent(typeof(Camera))]
public class MoveCameraOnFocus : MonoBehaviour {

     public Camera camera;
     public float minDistanceToChange = 1f;
     public Vector3 lastPosition;
     public float speed=1;
     
	void Start () {
        camera = FindObjectOfType<Camera>() as Camera;
	}
	
	void FixedUpdate () {
        
        if (CameraFocus.Instance == null ) return;
        CameraFocusElement focus = CameraFocus.Instance.currentFocus;
        if (focus == null) return;

        Vector3 newPosition =focus.GetCameraPosition();
        newPosition.z = transform.position.z;
        float distOldToNew = Vector3.Distance(newPosition, lastPosition);

        float d = Vector3.Distance(newPosition, lastPosition);
        bool useNewOne = d > minDistanceToChange;

        camera.transform.position = Vector3.MoveTowards(camera.transform.position, useNewOne ? newPosition : lastPosition, speed*Time.deltaTime);
        
        camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, focus.GetCameraSize(), Time.fixedDeltaTime);

        if (useNewOne) lastPosition = newPosition; 
	}
}
