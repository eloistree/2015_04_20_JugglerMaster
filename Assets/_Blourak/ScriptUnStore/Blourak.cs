﻿using UnityEngine;
using System.Collections;

public class Blourak : MonoBehaviour {

    public MultiDimBelong multiDim;
    public Dimensions dimension;
    public Dimensions.DimType lastDimension;
    public Dimensions.DimType currentDimension;
    public SpriteRenderer sr;
    public void Start() {
        if (!dimension) dimension = Dimensions.Instance;
        if (!dimension) { Destroy(this); return; }
        else 
        {
            //multiDim.SetAsOnly(currentDimension);
            sr.color = Dimensions.GetColor(lastDimension);
            dimension.AfterSwitch += (root, color, o, n) => {

                if (lastDimension == currentDimension) return;
                currentDimension = n; lastDimension = o;

                sr.color = Dimensions.GetColor(lastDimension);
            };
        }

    
    }

    void OnCollisionEnter2D(Collision2D col) {

        if ( ! col.gameObject.tag.Equals("LivingElement")) return;
        
        MultiDimBelong livingDim = col.gameObject.GetComponent<MultiDimBelong>() as MultiDimBelong;
        //print(">:"+livingDim);
        if (livingDim)
        {
            livingDim.AddTo(lastDimension);
        }
    }
}
