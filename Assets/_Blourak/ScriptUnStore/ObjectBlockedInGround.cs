﻿using UnityEngine;
using System.Collections;

public class ObjectBlockedInGround : MonoBehaviour {

    public float checkEverySec = 0.1f;
    public bool everyUpdate;

    private float countdown;
    public Checker shouldNotOverlapCollider;
    public bool isBlocked;
    public float since;
	
    public delegate void BlockInGround(Vector3 where, bool isBlocked);
    public BlockInGround onBlockInGround;
    
	void Update () {
        if (onBlockInGround == null) return;
        countdown -= Time.deltaTime;
        if (countdown <= 0f || everyUpdate) {
            countdown = checkEverySec;
            if (!isBlocked && shouldNotOverlapCollider.IsColliding2D())
            {
                isBlocked = true;
                onBlockInGround(transform.position, true);
            }
            else if (isBlocked && !shouldNotOverlapCollider.IsColliding2D())
            {
                isBlocked = false;
                since=0f;
                onBlockInGround(transform.position, false);
            }
            
        }

        if (isBlocked) 
        {
            since += Time.deltaTime;
        }
	
	}


}
