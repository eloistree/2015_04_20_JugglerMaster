﻿using UnityEngine;
using System.Collections;

public class OnWallOverlapbecomeEvil : MonoBehaviour {


    public MultiDimBelong multiDim;
    public InWallWhenSwitchDim isInWall;

    public void Start() 
    {
        if (isInWall && multiDim)
        {

            isInWall.onOverlapingDetected += () =>
            {
                if (multiDim)
                {
                    multiDim.SetAsOnly(Dimensions.DimType.Black);
                }
            };
        }
        else { Debug.LogWarning("Params not correctly initialised", this); Destroy(this); return; }

    }
}
