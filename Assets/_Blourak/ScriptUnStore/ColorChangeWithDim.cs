﻿using UnityEngine;
using System.Collections;

public class ColorChangeWithDim : MonoBehaviour {
    
    public MultiDimBelong multiDimension;
    public SpriteRenderer spriteRender;


    public float timeBeforeStargeChanging = 0f;
    public float timeToChange = 3f;
    public float countDown = 0f;
    public float startCountDown = 0f;
    public Color currentColor;
    public Color fromColor;
    public Color nextColor;
    public bool changeImmediatAtStart = true;
    public bool noLerpOnColor = true;

    public bool withFixeAlpha;
    public float fixedAlpha=255f;

    
    void Start () {
        if (multiDimension == null || spriteRender == null) { Destroy(this); return; }
     
        Color colorAtStart = Dimensions.GetColor(multiDimension.GetOriginalDimension());
        fromColor = colorAtStart;
        currentColor = colorAtStart;
        ChangeColor(colorAtStart, changeImmediatAtStart);
        multiDimension.onBelongToNewDimension += (md, type, color) =>
        {
            ChangeColor(Dimensions.GetColor(multiDimension.GetBelongTo()),false);
        };

  }

    void Update()
    {

        if (startCountDown > 0) {
            startCountDown -= Time.deltaTime;
            return;
        } 

        if(countDown<=0)return;
        if(nextColor==currentColor)return;
        countDown-=Time.deltaTime;
        
        currentColor = Color.Lerp(fromColor, nextColor, Mathf.Clamp((timeToChange-countDown) / timeToChange, 0f, 1f));


        spriteRender.color = currentColor;
        if (countDown <= 0)
        {
            ChangeColor(nextColor, true);
        }  
	}


    public void OnEnable() 
    {

        ChangeColor(Dimensions.GetColor(multiDimension.GetBelongTo()), true);
    }
    public void ChangeColor(Color next,bool immediately) 
    {
        if (withFixeAlpha)
            next.a = fixedAlpha;
        nextColor = next;


        if (immediately || noLerpOnColor)
        {

            currentColor = nextColor;
            fromColor = nextColor;
            countDown = 0;

            spriteRender.color = currentColor;
        }
        else {
            countDown = timeToChange;
            startCountDown = timeBeforeStargeChanging;
        }
    }

    public void SetAlpha(float pourcent)
    {
        fixedAlpha = Mathf.Clamp(pourcent, 0f, 1f);
    }
}
