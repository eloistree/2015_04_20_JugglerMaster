﻿using UnityEngine;
using System.Collections;

public class RemoveDimensions : MonoBehaviour {



    public void OnTriggerEnter2D(Collider2D col) 
    {
        if (!col.gameObject.tag.Equals("Projectile")) return;

        MultiDimBelong dims = col.gameObject.GetComponent<MultiDimBelong>() as MultiDimBelong;
        if (dims)
        {
            dims.ResetDim();
        }
    
    }
}
