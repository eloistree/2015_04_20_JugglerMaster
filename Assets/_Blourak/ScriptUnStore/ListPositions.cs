﻿using UnityEngine;
using System.Collections;

public class ListPositions : Positions
{

    public Transform[] positions;
    public int cursor;
    public override Vector3 GetPosition()
    {
        if (positions != null || positions.Length <= 0)
            return transform.position;
        Transform pos = positions[cursor];
        return pos == null ? transform.position : pos.position;
    }

    public override void Next()
    {
        cursor = (cursor+1) % positions.Length;
    }
}

