﻿using UnityEngine;
using System.Collections;

public class BlourakDimSwitchVsWallBlock : MonoBehaviour {

    public BlourakController blourak;
    public Rigidbody2D blourakRigidBody2D;
    public Dimensions switchDimension;
    public Checker blockGroundDetector;
    private Vector3 velocityBeforeCheck;

   public void Start()
   {
       if (blourak != null && blockGroundDetector != null && switchDimension != null && blourakRigidBody2D != null)
       {
           switchDimension.BeforeSwitch += (root, color,from,to) =>
           {
               if (blourakRigidBody2D != null)
               {
                   velocityBeforeCheck = blourakRigidBody2D.velocity;
                   blourakRigidBody2D.isKinematic = true;
                 //  Debug.Break();
               }
           };

           switchDimension.AfterSwitch += (root, color, from, to) =>
           {
                bool blocked = blockGroundDetector.IsColliding2D();
                   
                blourak.SetAsBlocked(blocked);

                   if (blourakRigidBody2D != null)
                   {
                       blourakRigidBody2D.isKinematic = blocked;
                       if(!blocked)
                           blourakRigidBody2D.velocity = velocityBeforeCheck;
                   }
           };

       }
       Destroy(this);

   }

}
