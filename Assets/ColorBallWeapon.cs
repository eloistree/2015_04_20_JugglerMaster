﻿using UnityEngine;
using System.Collections;

public class ColorBallWeapon : MonoBehaviour, IWeapon {


    public Transform        bulletStartPoint;
    public BulletFactory    bulletsPool;
    public IDimAmmoLoader   ammoLoader;
    public float            bulletPower = 20f;

    public MonoBehaviour sc_ammoLoader;

    void Start() {
        if (sc_ammoLoader != null) {
            ammoLoader = (IDimAmmoLoader) sc_ammoLoader;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T)) {
            

            Fire();
        }
        
    }

    public bool Fire()
    {
       return Fire(bulletStartPoint.forward);
    }

    public bool Fire(Vector3 direction)
    {
        direction.Normalize();
        GameObject bullet = bulletsPool.GetNextAvailable();
        if (bullet == null) { Debug.Log("No bullets available in pool"); return false; }
        Rigidbody2D rigBody = bullet.GetComponent<Rigidbody2D>();
        if (rigBody == null) { Debug.Log("No rigidbody 2D on the bullet"); return false; }        
        if (ammoLoader == null) { Debug.Log("No ammo manager"); return false; }
        int ammoCount =ammoLoader.GetAmmoCount();
        if (ammoCount<=0) { Debug.Log("No ammo"); return false; }
        int ammoIndex = ammoCount - 1;


        Dimensions.DimType[] colors = ammoLoader.GetColors(ammoIndex);
        MultiDimBelong bulletDimension = bullet.GetComponent<MultiDimBelong>() as MultiDimBelong;
        if (colors.Length>0 && bulletDimension!=null) {
            bulletDimension.ResetDimWith(colors);
        }
        ammoLoader.UseAmmo();

        bullet.SetActive(true);
        bullet.transform.position = bulletStartPoint.position;
        rigBody.AddForce(direction * bulletPower,ForceMode2D.Impulse);
        return true;
    }

    public bool HasAmmo()
    {
        if (ammoLoader == null) return false;
        return ammoLoader.GetAmmoCount() > 0;
    }

    public int GetAmmoCount()
    {
        if (ammoLoader == null) return 0;
        return ammoLoader.GetAmmoCount();
    }

    public IAmmoLoader GetAmmoLoader()
    {
        return ammoLoader;
    }
}

