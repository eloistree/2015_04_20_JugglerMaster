﻿using UnityEngine;
using System.Collections;

public class StationCollisionToAmmoColor : MonoBehaviour {


    public string stationTagName = "ColorStation"; 
    public IDimAmmoLoader ammoLoader;
    public MonoBehaviour sc_ammoLoader;
    
    void Start()
    {
        if (sc_ammoLoader != null)
        {
            ammoLoader = (IDimAmmoLoader)sc_ammoLoader;
        }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {

        //Debug.Log(col.gameObject.tag);
        if (col.gameObject.CompareTag(stationTagName))
        {
            DimensionStation station = col.gameObject.GetComponent<DimensionStation>() as DimensionStation;
            if (station != null)
            {
                ammoLoader.AddColors(new Dimensions.DimType[]{ station.dimension});

            }
        }

    }
}
