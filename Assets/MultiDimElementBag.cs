﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiDimElementBag : MonoBehaviour {

    public List<MultiDimBelong> elements = new List<MultiDimBelong>();
    public int cursor = 3;

    void Start() {
        cursor =  elements.Count;
    }
    public bool Set(int index, MultiDimBelong dim) {
        if (index < 0 || index >= elements.Count) return false ;
            elements[index].ResetDim();
            elements[index].AddDim(dim);
            return true;
    }
    public MultiDimBelong Get(int index)
    {
        if (index < 0 || index >= elements.Count) return null;
        return elements[index];
    }

    public void Set(int index)
    {
        if (index < 0 || index >= elements.Count) return;
            elements[index].ResetDim();
    }

    public int Count { get { return elements.Count; } }
}
