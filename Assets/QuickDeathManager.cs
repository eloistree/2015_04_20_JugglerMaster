﻿using UnityEngine;
using System.Collections;

public class QuickDeathManager : MonoBehaviour {


    public BossLife bossLife;
    public PlayerColoredLife [] playerLife;
    public float timeBeforeScreenDisplay = 2f;

    public GameObject[] godWin;
    public GameObject[] playerWin;

    void Start() {
        playerLife  = GameObject.FindObjectsOfType<PlayerColoredLife>() as PlayerColoredLife[];
        bossLife    = GameObject.FindObjectOfType<BossLife>() as BossLife;
    }

    void Update () {
        if (bossLife != null)
            if (bossLife.GetClarityPourcent() >= 1f)
            {
                Invoke("PlayersWin", timeBeforeScreenDisplay);
            }
        if (playerLife != null && playerLife.Length > 0) {
            bool playerAllDeath = true;
            foreach (PlayerColoredLife life in playerLife)
                playerAllDeath &= life.GetDarknessPourcent() >= 1f;

            if (playerAllDeath)
            {
                Invoke("BossWin", timeBeforeScreenDisplay);
            }
        }
            
	
	}

    private void BossWin()
    {
        foreach (GameObject go in godWin)
            go.SetActive(true);

    }

    private void PlayersWin()
    {

        foreach (GameObject go in playerWin)
            go.SetActive(true);
    }
}
