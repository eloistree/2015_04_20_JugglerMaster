﻿using UnityEngine;
using System.Collections;

public class QuickDisplayJuggleBalls : MonoBehaviour {

    public Juggler jugglerInfo;
    public Transform[] tBalls;
    public Transform tHandLeft;
    public Transform tHandRight;


    void Update() {

        float zDepthInScene = 2f;
        Juggler ji = jugglerInfo;
        //Positionner
        for (int i = 0; i < ji.balls.Length; i++)
        {
            if (ji.balls[i].IsUsed)
            {
                Vector3 position = ji.GetBallSupposedPosition(i);
                position.z = zDepthInScene;
                tBalls[i].position = Camera.main.ViewportToWorldPoint(position);
                tBalls[i].gameObject.SetActive(true);
            }
            else
            {
                tBalls[i].gameObject.SetActive(false);
            }
        }

        Vector3 leftPos = ji.left._normalizedPosition;
        leftPos.z = zDepthInScene;
        tHandLeft.position = Camera.main.ViewportToWorldPoint(leftPos);

        Vector3 rightPos = ji.right._normalizedPosition;
        rightPos.z = zDepthInScene;
        tHandRight.position = Camera.main.ViewportToWorldPoint(rightPos);
    }
}
