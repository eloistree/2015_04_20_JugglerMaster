﻿using UnityEngine;
using System.Collections;

public class BulletFactory : MonoBehaviour {

    public int poolCount = 30;
    public float bulletSize=1f;
    public GameObject bulletPrefab;
    public GameObject [] bullets;

	void Start () {
        bullets = new GameObject[poolCount];
        for (int i = 0; i < bullets.Length; i++)
        {
            bullets[i] = GameObject.Instantiate(bulletPrefab) as GameObject;
            bullets[i].name = "Bullet_" + i;
            bullets[i].transform.localScale = Vector3.one * bulletSize;
            bullets[i].SetActive(false);
            bullets[i].transform.parent = this.transform;
        }
	}

    public GameObject GetNextAvailable() {
        for (int i = 0; i < bullets.Length; i++)
            if ( ! bullets[i].activeSelf)
                return bullets[i];
        return null;
    
    }
	
}
