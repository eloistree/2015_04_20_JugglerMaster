﻿using UnityEngine;
using System.Collections;

public class PlayerColoredLife : ColoredLife {


    public SpriteRenderer [] lifeReferenceSprite;

    protected override void RefreshLifeColor(Color life)
    {
        base.RefreshLifeColor(life);
            foreach(SpriteRenderer spr in lifeReferenceSprite)
                spr.color = life;
    }

}
