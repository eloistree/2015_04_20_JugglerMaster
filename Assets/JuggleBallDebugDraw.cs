﻿using UnityEngine;
using System.Collections.Generic;

public class JuggleBallDebugDraw : MonoBehaviour {

    public Juggler juggler;
    public Dictionary<Juggler.JuggleElement, Queue<Vector2>> paths= new Dictionary<Juggler.JuggleElement,Queue<Vector2>>();
    public Dictionary<Juggler.JuggleElement, Vector2> trackers=new Dictionary<Juggler.JuggleElement,Vector2>();
	// Use this for initialization

    public float trackerSpeed = 0.5f;
	void Start () {

        foreach (Juggler.JBall ball in juggler.balls)
        {
            //           ball.onPathEnd += DrawPath;
                       ball.onPathNewPoint += AddPathPoint;
        }
	}

    void OnDestroy() {
        foreach (Juggler.JBall ball in juggler.balls)
        {
            //          ball.onPathEnd -= DrawPath;
            ball.onPathNewPoint -= AddPathPoint;
        }
    }

    void Update() {

        foreach (Juggler.JuggleElement elem in paths.Keys) {
            Queue<Vector2> path = paths[elem];
            Vector2 tracker = trackers[elem];

            Vector2 [] pathArray = path.ToArray();
            if (pathArray.Length > 0) {
                Vector2 newTrackerPos = Vector2.MoveTowards(tracker, pathArray[0], Time.deltaTime * trackerSpeed);
                trackers[elem] = newTrackerPos;
                if (Vector2.Distance(newTrackerPos, pathArray[0]) == 0f)
                    path.Dequeue();
            }
            DrawLine(tracker, pathArray, Color.red);
        }
    }

    private void DrawLine(Vector2 tracker, Vector2[] line, Color color)
    {
        if(line.Length==0) return;
        Debug.DrawLine(GetRealPoint(tracker), GetRealPoint(line[0]), color);
        if(line.Length>1)
        for (int i = 1; i < line.Length; i++)
        {
            Debug.DrawLine(GetRealPoint( line[i - 1]), GetRealPoint(line[i]), color);
        
            
        }
    }

    private void AddPathPoint(Juggler.JuggleElement element, Vector2 normPosition, float when)
    {
        if (!paths.ContainsKey(element)) { 
            paths.Add(element, new Queue<Vector2>());
            trackers.Add(element, new Vector2());
        }
        Queue<Vector2> path;
        paths.TryGetValue(element, out path);
        if (path != null)
            path.Enqueue(normPosition);
    }

    public float displayTime = 60f;

    private void DrawPath(Juggler.JuggleElement element, Vector2[] pathOfNormPosition, float duration)
    {
        for (int i = 0; i < pathOfNormPosition.Length; i++)
        {
            if (i + 1 < pathOfNormPosition.Length) {

                Vector3 from = Camera.main.ViewportToWorldPoint(pathOfNormPosition[i]);
                Vector3 to = Camera.main.ViewportToWorldPoint(pathOfNormPosition[i + 1]);
                float pourcent = (float)i / (float)pathOfNormPosition.Length;
                Debug.DrawLine(from, to, ColorsGetter.GetAllColorFor(pourcent), displayTime);
            
            }
            
        }
    }

    public Vector3 GetRealPoint(Vector2 normPoint) {
        return Camera.main.ViewportToWorldPoint(normPoint);
    
    }
	
}
